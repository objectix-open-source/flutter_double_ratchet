# Glossary

In this section we want to ingtroduce and discuss some key terms and the concepts behind those terms as used in this package.

## Applications and Application Namespace

An "Application" is any software program that can be used to produce or consume digital messages that are exchanged between Users.

Instances (i. e. processes) of the same Application running on arbitrary devices share a common "Application Namespace" that uniquely identifies that Application.

## User

A "User" is a technical entity that produces or consumes messages on one or more devices (e.g. smartphone, tablet, desktop PC, notebook) using an Application. 

This for instance could be a human user or an automated process, such as an aggregation service that reads from multiple data streams, filters for relevant data and publishes the resulting data for consumption through other users.

## Endpoint

An "Endpoint" refers to an Application running on a *specific* device operated by a *particular* User.

For instance a messenger application running on a smartphone and a tablet of a User defines two Endpoints, one for the messenger application running on the smartphone and another Endpoint for the messenger application on the tablet.

An Endpoint is always bound to a particular User.

## Identity

An "Identity" is technical entity that uniquely identifies a specific Endpoint and allows to specifically address that Endpoint for communication purposes. Such an Endpoint is owned or operated by a User and belongs to a specific application namespace.

> **Important:** Users and Identities are two different things: while a User refers to a consumer/producer of messages, an Identity refers to one particular application  running on a specific device operated by a User. 
> 
> In this way a User may have one or more Identities associated. Vice versa, a particular Identity has exactly one User attached to it (i.e. an identity cannot be shared between two or more Users).

An Identity's stringified address is then denoted by the following string format:

```
USER_ADDRESS/APPLICATION_NAMESPACE/ENDPOINT_ID
```
 
*USER_ADDRESS* denotes an address following the format outlined in [section 3.4.1 of RFC 5322](https://datatracker.ietf.org/doc/html/rfc5322#section-3.4.1). Example: "some-user@example.com". This approach allows for federation of users across an unlimited set of servers, very similar to the e-mail servers available on the Internet.

*APPLICATION_NAMESPACE* identifies an application namespace in reverse domain notation (z.y.x...), which uniquely identifies the Application the endpoint is implemented by. Example: "de.objectix.my-messenger".

*ENDPOINT_ID* uniquely refers to a specific Endpoint operated by the User through a [URL-safe Base64-encoding](https://www.rfc-editor.org/rfc/rfc4648#section-5) of a [UUIDv4](https://www.rfc-editor.org/rfc/rfc4122) value (with padding removed). Example: "JryUWQi4Swews3Gf5hsIyg". 

> Please note that this ID **MUST be globally unique** for all Endpoints, regardless of the User and Application Namespace.

The full stringified address of an Identity assuming the examples given above would result in the following Identity address:

```
some-user@example.com/de.objectix.my-messenger/JryUWQi4Swews3Gf5hsIyg
```

> **Identities and multi-tenancy:** if the same Application on the same device is used by multiple Users, it is mandatory to use different Identities for each such User, which specifically means to use different user addresses AND endpoint identifiers.

## Communication Groups

The diagram below shows a typical use case with three users Alice, Bob and Charles (blue). Alice and Bob operate a smartphone and a tablet, while Charles operates smartphone only.

![Identities in a typical scenario](glossary-identities.png)

Alice, Bob and Charles operate an application 1 one all their respective smartphone and tablet devices, thus resulting in a total of five Identities (green) that form a communication group CG<sub>1</sub> := [Id<sub>2</sub>, Id<sub>3</sub>, Id<sub>4</sub>, Id<sub>5</sub>, Id<sub>7</sub>] (yellow).

Alice and Bob operate another application 2 on their respective smartphones, resulting in two additional Identities (green) that form a second communication group CG<sub>2</sub> := [Id<sub>1</sub>, Id<sub>6</sub>] (orange).

Identities for each application be distinguished by their assigned application namespace (denoting the communication group) and endpoint identifiers (denoting the specific endpoint, i.e. application instance on a device). Thus, an Identity can never be part of more than one communication group at the same time. In other words, communication groups are always disjunct.

## Private Groups

It is important to mention that each application needs to implement their own logical view on communication groups groups, allowing their individual users to limit communication to private groups between individual members of the communication group. 

For instance application 1 could put Alice and Charles into a private group PG := [Id<sub>2</sub>, Id<sub>3</sub>, Id<sub>7</sub>] (dashed pink lines in diagram above), limiting communication within that private group to those Identities. Thus, if Alice and Bob exchange messages within PG, those messages would be readable only by Identities contains in PG. Identities associated with Bob would not receive any of those messages.

If Alice later on decides to add Bob to the private group, it essentially would need to add Bob's respective Identities to the private group, eventually forming PG' := [Id<sub>2</sub>, Id<sub>3</sub>, Id<sub>4</sub>, Id<sub>5</sub>, Id<sub>7</sub>].

In that way a private group is always a subset of a communication group, limiting exchange of messages to the Identities that are members of the respective private group.

The technical process of forming and implementing those private groups is beyond the scope of this library, as this is strictly specific to a particular application. However, by defining the structure of a private group, this library performs the heavy work of broadcasting a message to all identities of the private group in a secure manner.