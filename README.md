<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->

Implements a secure communication layer based on the Double Ratchet algorithm, X3DH protocol and Sesame algorithm proposed by Trevor Perrin and Moxie Marlinspike in 2013.

## Features

Implements

- X3DH (Triple-Diffie-Hellman) Key exchange for setting up
- Double Ratchet algorithm for exchanging encrypted messages
- Sesame for managing secure sessions across different devices of correspondents

Supports

- simple use through a facade
- flexible configuration through service implementations to match specific needs (such as integrating your own identity and signalling servers)

## Getting started

The easiest approach for setting up a (bi-directional) secure session between two parties A(lice) and B(ob) is to use the Session Builder. This requires implementation of various interfaces:

- KeyStore is required for storing key material securely on the local device. If in-memory storage for key material is sufficient, you can stick with the X3DHInMemoryKeyStorage provided with this package.

- KeyProvider implements generation of fresh key material for use with the X3DH and Double Ratchet Algorithm. You can use the Key Provider implementation that comes with this package unless you want to use different means to create the required key material (such as using a cryptoprocessor).

- KeyManager implements retrieval or generation of key material for use with the X3DH and Double Ratchet Algorithm. Usually, you must implement your own key package publisher to integrate a specific identity service used by your software system.

- MessagingService performs the actual reliable data transport from a client to a remote endpoint. You need to implement your own message service in order to integrate your own messaging server.

BlackSun provides default implementations for all aforementioned interfaces intended for use with Solar.

```dart
// Create identity of Alice, representing a specific device owned
// by Alice, which runs an appliction, whose identifier is given
// as part of the identity.
final localIdentity = Identity.createNew(
    User(address: "alice@example.com"),
    "test.example.com",
);

// Create required services for key storage, generation and 
// key management, as well as message transport. Here we use 
// the default service implementations provided by BlackSun 
// for Solar.
final solarEnvironment = Environment.createForSolar();

// Create the BlackSun facade
final blackSun = BlackSun(environment: solarEnvironment);

// If the local device has not been setup before, we set it up for use wit BlackSun.
await blackSun.setupLocalDevice(localIdentity);

// We then can setup a Dart stream for inbound messages we 
// want to receive from the Internet. Those messages contain
// the plaintext of encrypted messages received via messaging
// system.
final inboundMessages = await blackSun.connectToMailbox();

// Listen to the inbound message stream and process incoming
// messages as per application's requirements.
inboundMessages.listen((InboundMessage message) {
    print("Sent by user: ", message.sender.user.address);
    print("Sent from device: ", message.sender.id)
    print("Received at timestamp: ", message.receivedAt);
    print(message.plaintext);
});

// Suppose that Alice want to send messages to another
// BlackSun user "Bob". To do so, we need to add Bob as a
// correspondent. This basically adds this user into our 
// local contact addressbook, allowing us to send him messages 
// later on.
final userBob = User(address: "bob@acme.org")
await blackSun.addCorrespondent(userBob);

// Alice constructs a binary message she wants to send to Bob.
List<int> messageContent = "Hello Bob".codeUnits;
final Uint8List message = Uint8List.fromList(messageContent);

// Alice then sends the message to Bob. BlackSun will take
// care of encrypting the message properly before sending it
// to Bob.
await blackSun.send(message, [userBob]);
```

## Usage

```dart
TBD
```

## Additional information

TODO: Tell users more about the package: where to find more information, how to
contribute to the package, how to file issues, what response they can expect
from the package authors, and more.
