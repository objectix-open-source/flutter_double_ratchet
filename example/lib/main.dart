// ignore_for_file: avoid_print

import 'package:flutter/foundation.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';

void main() async {
  final identityAlice = Identity.createNew(
    User(address: "alice@example.com"),
    "test.example.com",
  );
  final identityBob = Identity.createNew(
    User(address: "bob@example.com"),
    "test.example.com",
  );

  // Setup the required servics for each identity. In this case, we use the
  // mock infrastructure.
  final environmentAlice = Environment.createForTest(identityAlice);
  final environmentBob = Environment.createForTest(identityBob);

  // Start with setting up Alice' device.
  final alice = BlackSun(environment: environmentAlice);
  await alice.setupLocalDevice(identityAlice);
  final inboundMessagesAlice = await alice.connectToMailbox();

  // Then setup Bob's device.
  final bob = BlackSun(environment: environmentBob);
  await bob.setupLocalDevice(identityBob);
  final inboundMessagesBob = await bob.connectToMailbox();

  // Because Alice want to initiate a message to Bob, we need to
  // add Bob and his devices to Alice' device and prepare them for
  // encryption.
  await alice.addCorrespondent(identityBob.user);

  // Listen to messages arriving at Alice' mailbox.
  inboundMessagesAlice.listen(
    (InboundMessage messageReceived) {
      print("Alice received message");
      print(messageReceived.plaintext);
    },
  );

  // Listen to messages arriving at Bob's mailbox.
  inboundMessagesBob.listen(
    (InboundMessage messageReceived) async {
      print("Bob received message");
      print(messageReceived.plaintext);
    },
  );

  // Alice constructs test message M, which Alice sends to Bob.
  List<int> codeUnitsM = "Hello Bob".codeUnits;
  final Uint8List messageM = Uint8List.fromList(codeUnitsM);

  // Send message M to Bob
  await alice.send(messageM, [identityBob.user]);

  // Wait for a message to arrive, before Bob sends back a response.
  await inboundMessagesBob.isEmpty;

  // Create a new test message N, which Bob sends back to Alice.
  List<int> codeUnitsN = "Hello Alice".codeUnits;
  final Uint8List messageN = Uint8List.fromList(codeUnitsN);

  // Send message N back to Alice
  await bob.send(messageN, [identityAlice.user]);
}
