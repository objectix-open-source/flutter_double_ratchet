// This is an example unit test.
//
// A unit test tests a single function, method, or class. To learn more about
// writing unit tests, visit
// https://flutter.dev/docs/cookbook/testing/unit/introduction

import 'dart:math';

import 'package:cryptography/cryptography.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet_message.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet_exception.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet_session_state.dart';

class _DoubleRatchetTestSessionPair {
  final _DoubleRatchetTestAppSession session1;
  final _DoubleRatchetTestAppSession session2;

  _DoubleRatchetTestSessionPair({required this.session1, required this.session2});
}

class _DoubleRatchetTestAppSession {
  final DoubleRatchetSessionState state = DoubleRatchetSessionState();
  final DoubleRatchet doubleRatchet = DoubleRatchet();
  final SimpleKeyPair dhKeyPair;

  _DoubleRatchetTestAppSession(this.dhKeyPair);

  static Future<_DoubleRatchetTestSessionPair> setupSessionPair() async {
    final alice = _DoubleRatchetTestAppSession(await X25519().newKeyPair());
    final bob = _DoubleRatchetTestAppSession(await X25519().newKeyPair());

    // Simulate a DH key exchange between Alice and Bob to derive a shared
    // secret that is required during session setup.
    final sharedSecret = await X25519().sharedSecretKey(
      keyPair: alice.dhKeyPair,
      remotePublicKey: await bob.dhKeyPair.extractPublicKey(),
    );

    // Setup sessions for Alice and Bob
    await alice.doubleRatchet.initSessionAsLocal(
      alice.state,
      sharedSecret,
      await bob.dhKeyPair.extractPublicKey(),
    );

    await bob.doubleRatchet.initSessionAsRemote(
      bob.state,
      sharedSecret,
      bob.dhKeyPair,
    );

    return _DoubleRatchetTestSessionPair(session1: alice, session2: bob);
  }
}

void main() {
  group('Double Ratchet', () {
    const chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    final Random rnd = Random();

    String getRandomString(int length) =>
        String.fromCharCodes(Iterable.generate(length, (_) => chars.codeUnitAt(rnd.nextInt(chars.length))));

    test('should encrypt and decrypt two-way message', () async {
      // Scenario: Alice wants to send encrypted message to Bob
      final sessions = await _DoubleRatchetTestAppSession.setupSessionPair();
      final alice = sessions.session1;
      final bob = sessions.session2;

      // Now, let Alice encrypt a message.
      final plaintext = "This is some secret message.".codeUnits;
      final message = await alice.doubleRatchet.encrypt(
        alice.state,
        Uint8List.fromList(plaintext),
        Uint8List.fromList("test".codeUnits),
      );

      // Assume, the ciphertext has been sent to Bob via network. Bob now
      // decrypts the data to retrieve the original message.
      final original = await bob.doubleRatchet.decrypt(
        bob.state,
        message,
        Uint8List.fromList("test".codeUnits),
      );

      // Assert that the decrypted message is identical to the encrypted message
      expect(original, plaintext);

      // Now let Bob respond with another message to Alice.
      final plaintext2 = "This is an encrypted response.".codeUnits;
      final message2 = await bob.doubleRatchet.encrypt(
        bob.state,
        Uint8List.fromList(plaintext2),
        Uint8List.fromList("test".codeUnits),
      );

      // Assume that the ciphertext has been sent back to Alice. She now
      // decrypts the data to retrieve the original response.
      final original2 = await alice.doubleRatchet.decrypt(
        alice.state,
        message2,
        Uint8List.fromList("test".codeUnits),
      );

      // Assert that the decrypted message is identical to the encrypted message
      expect(original2, plaintext2);
    });

    test('should encrypt and decrypt two-way message in reverse direction', () async {
      // Scenario: After Alice forms a session with Bob, he directly sends
      // a message to Alice.
      final sessions = await _DoubleRatchetTestAppSession.setupSessionPair();
      final alice = sessions.session1;
      final bob = sessions.session2;

      // Now, let Bob encrypt a message.
      final plaintext = "This is some secret message.".codeUnits;
      final message = await bob.doubleRatchet.encrypt(
        bob.state,
        Uint8List.fromList(plaintext),
        Uint8List.fromList("test".codeUnits),
      );

      // Assume, the ciphertext has been sent to Alice via network. Alice now
      // decrypts the data to retrieve the original message.
      final original = await alice.doubleRatchet.decrypt(
        alice.state,
        message,
        Uint8List.fromList("test".codeUnits),
      );

      // Assert that the decrypted message is identical to the encrypted message
      expect(original, plaintext);

      // Now let Alice respond with another message to Bob.
      final plaintext2 = "This is an encrypted response.".codeUnits;
      final message2 = await alice.doubleRatchet.encrypt(
        alice.state,
        Uint8List.fromList(plaintext2),
        Uint8List.fromList("test".codeUnits),
      );

      // Assume that the ciphertext has been sent back to Alice. She now
      // decrypts the data to retrieve the original response.
      final original2 = await bob.doubleRatchet.decrypt(
        bob.state,
        message2,
        Uint8List.fromList("test".codeUnits),
      );

      // Assert that the decrypted message is identical to the encrypted message
      expect(original2, plaintext2);
    });

    test('should encrypt and decrypt in complex two-way communication', () async {
      // Scenario: After Alice forms a session with Bob, they exchange random
      // messages in arbitrary order.
      final sessions = await _DoubleRatchetTestAppSession.setupSessionPair();
      final alice = sessions.session1;
      final bob = sessions.session2;

      // Remember the message sent and received by Alice and Bob, respectively.
      final List<Uint8List> aliceSent = [];
      final List<Uint8List> bobSent = [];
      final List<Uint8List> aliceReceived = [];
      final List<Uint8List> bobReceived = [];

      // We exchange a 1000 random messages in total, using a randomized
      // communication scheme.
      for (var i = 0; i < 1000; i++) {
        // Determine sender and receiver for current message
        _DoubleRatchetTestAppSession sender;
        _DoubleRatchetTestAppSession receiver;

        // Construct a random message
        final randomMessageBytes = Uint8List.fromList(getRandomString(2000).codeUnits);
        final rndValue = Random().nextBool();

        if (rndValue) {
          // Alice becomes sender, Bob becomes receiver
          sender = alice;
          receiver = bob;
          aliceSent.add(randomMessageBytes);
        } else {
          // Alice becomes receiver, Bob becomes sender
          sender = bob;
          receiver = alice;
          bobSent.add(randomMessageBytes);
        }

        // Now encrypt the message
        final message = await sender.doubleRatchet.encrypt(
          sender.state,
          Uint8List.fromList(randomMessageBytes),
          Uint8List.fromList("test".codeUnits),
        );

        // Assume, the ciphertext has been sent via network. Receiver now
        // decrypts the data to retrieve the original message.
        final original = await receiver.doubleRatchet.decrypt(
          receiver.state,
          message,
          Uint8List.fromList("test".codeUnits),
        );

        expect(original, randomMessageBytes);

        if (rndValue) {
          bobReceived.add(Uint8List.fromList(original));
        } else {
          aliceReceived.add(Uint8List.fromList(original));
        }
      }

      // Now, after exchanging all the messages, we check that sent and
      // received items are identical on both sides.
      expect(aliceSent.length, bobReceived.length);
      for (var i = 0; i < aliceSent.length; i++) {
        expect(listEquals(aliceSent[i], bobReceived[i]), true);
      }

      expect(aliceReceived.length, bobSent.length);
      for (var i = 0; i < bobSent.length; i++) {
        expect(listEquals(bobSent[i], aliceReceived[i]), true);
      }
    });

    test('should encrypt and decrypt two-way message with message skipping', () async {
      // Scenario: Alice sends a few messages to Bob, which are assumed to be
      // ignored by him.
      final sessions = await _DoubleRatchetTestAppSession.setupSessionPair();
      final alice = sessions.session1;
      final bob = sessions.session2;
      final plaintext = "This is some secret message.".codeUnits;

      // We simulate loss of maxSkippableMessages - 1 messages. This should
      // allow Bob to catch up with messages sent afterwards.
      for (var i = 0; i < bob.state.maxSkippableMessages - 1; i++) {
        await alice.doubleRatchet.encrypt(
          alice.state,
          Uint8List.fromList(plaintext),
          Uint8List.fromList("test".codeUnits),
        );
      }

      // So far, Bob has ignored all messages so far. Now, Alice sends
      // another message to Bob.
      final message2 = await alice.doubleRatchet.encrypt(
        alice.state,
        Uint8List.fromList(plaintext),
        Uint8List.fromList("test".codeUnits),
      );

      // Assume, the second message is now received by Bob.
      final original = await bob.doubleRatchet.decrypt(
        bob.state,
        message2,
        Uint8List.fromList("test".codeUnits),
      );

      // Assert that the decrypted message is identical to the encrypted message
      expect(original, plaintext);

      // We simulate loss of maxSkippableMessages + 1 messages. This should
      // cause an error, once Bob attempts to decrypt a later message.
      for (var i = 0; i < bob.state.maxSkippableMessages + 1; i++) {
        await alice.doubleRatchet.encrypt(
          alice.state,
          Uint8List.fromList(plaintext),
          Uint8List.fromList("test".codeUnits),
        );
      }

      // So far, Bob has ignored all messages so far. Now, Alice sends
      // another message to Bob.
      final message3 = await alice.doubleRatchet.encrypt(
        alice.state,
        Uint8List.fromList(plaintext),
        Uint8List.fromList("test".codeUnits),
      );

      // Assume, the second message is now received by Bob. Because there
      // have been too many lost messages, decryption should fail here.
      await expectLater(
        bob.doubleRatchet.decrypt(
          bob.state,
          message3,
          Uint8List.fromList("test".codeUnits),
        ),
        throwsA(isA<DoubleRatchetException>()),
      );
    });

    test('should restore state after failed and keep updated state after successful decryption', () async {
      // Scenario: Alice wants to send encrypted message to Bob
      final sessions = await _DoubleRatchetTestAppSession.setupSessionPair();
      final alice = sessions.session1;
      final bob = sessions.session2;

      // Now, let Alice encrypt a message.
      final plaintext = "This is some secret message.".codeUnits;
      final message = await alice.doubleRatchet.encrypt(
        alice.state,
        Uint8List.fromList(plaintext),
        Uint8List.fromList("test".codeUnits),
      );

      // Assume, the ciphertext has been sent to Bob via network. We simulate
      // tampered network package, which cannot be decypted by Bob due to
      // failed message authentiation.
      final originalMessageCiphertext = List<int>.from(message.ciphertext.cipherText);
      message.ciphertext.cipherText.shuffle();

      // We remember Bob's current state as a copy.
      final bobsStateBefore = bob.state.copy();

      // Make sure that the decryption actually fails.
      await expectLater(
        bob.doubleRatchet.decrypt(
          bob.state,
          message,
          Uint8List.fromList("test".codeUnits),
        ),
        throwsException,
      );

      // Bob's current state should be the same as before the failed decrypt.
      expect(bob.state.localDH, equals(bobsStateBefore.localDH));
      expect(bob.state.maxSkippableMessages, equals(bobsStateBefore.maxSkippableMessages));
      expect(bob.state.previousSendingMessageNumber, equals(bobsStateBefore.previousSendingMessageNumber));
      expect(bob.state.receivingChainKey, equals(bobsStateBefore.receivingChainKey));
      expect(bob.state.receivingMessageNumber, equals(bobsStateBefore.receivingMessageNumber));
      expect(bob.state.remoteDH, equals(bobsStateBefore.remoteDH));
      expect(bob.state.rootKey, equals(bobsStateBefore.rootKey));
      expect(bob.state.sendingChainKey, equals(bobsStateBefore.sendingChainKey));
      expect(bob.state.sendingMessageNumber, equals(bobsStateBefore.sendingMessageNumber));
      expect(bob.state.skippedMessageKeys, equals(bobsStateBefore.skippedMessageKeys));

      // We now restore the actual cipertext as it was originally sent by Alice.
      final restoredMessage = DoubleRatchetMessage(
        ciphertext: SecretBox(
          originalMessageCiphertext,
          nonce: message.ciphertext.nonce,
          mac: message.ciphertext.mac,
        ),
        header: message.header,
      );

      // Make sure that the decryption now succeeds.
      await expectLater(
        bob.doubleRatchet.decrypt(
          bob.state,
          restoredMessage,
          Uint8List.fromList("test".codeUnits),
        ),
        completes,
      );

      // Assert, tat Bob's new state is now different from the original state
      expect(bob.state, isNot(equals(bobsStateBefore)));
    });
  });
}
