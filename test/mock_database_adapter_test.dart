import 'dart:typed_data';

import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/device_record.dart';
import 'package:flutter_black_sun/src/device-state/session_record.dart';
import 'package:flutter_black_sun/src/device-state/user_record.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet_session_state.dart';
import 'package:flutter_black_sun/src/services/mocks/mock_device_state_database_adapter.dart';
import 'package:flutter_black_sun/src/util/uuid_shortener.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:uuid/uuid.dart';

void main() {
  group("Mock Persistence Adapter", () {
    test("should manage all supported record types", () async {
      final adapter = MockDeviceStateDatabaseAdapter();

      final user = User(address: "test@example.com");
      final identityKey = await DefaultKeyProvider().generateKey();

      final identity = Identity.createNew(
        user,
        "de.objectix.test",
      );

      final userRecord = UserRecord(
        user: user,
      );

      final deviceRecord = DeviceRecord(
        userRecordId: userRecord.id,
        identity: identity,
        identityKey: await identityKey.extractPublicKey(),
      );

      final associatedData = "Some associated data".codeUnits;

      final sessionRecord = SessionRecord(
        id: const Uuid().v4short(),
        deviceRecordId: identity.id,
        state: DoubleRatchetSessionState(),
        associatedData: Uint8List.fromList(associatedData),
      );

      await adapter.storeRecord(userRecord);
      await adapter.storeRecord(deviceRecord);
      await adapter.storeRecord(sessionRecord);

      final UserRecord? fetchedUserRecord = await adapter.fetchRecord(userRecord.id);
      expect(fetchedUserRecord, equals(userRecord));

      final DeviceRecord? fetchedDeviceRecord = await adapter.fetchRecord(deviceRecord.id);
      expect(fetchedDeviceRecord, equals(deviceRecord));

      final fetchedSessionRecord = await adapter.fetchRecord(sessionRecord.id);
      expect(fetchedSessionRecord, equals(sessionRecord));

      final UserRecord? deletedUserRecord = await adapter.deleteRecord(userRecord.id);
      expect(deletedUserRecord, equals(userRecord));
      final UserRecord? notExistingUserRecord = await adapter.fetchRecord(deletedUserRecord!.id);
      expect(notExistingUserRecord, isNull);

      final DeviceRecord? deletedDeviceRecord = await adapter.deleteRecord(deviceRecord.id);
      expect(deletedDeviceRecord, equals(deviceRecord));
      final DeviceRecord? notExistingDeviceRecord = await adapter.fetchRecord(deletedDeviceRecord!.id);
      expect(notExistingDeviceRecord, isNull);

      final SessionRecord? deletedSessionRecord = await adapter.deleteRecord(sessionRecord.id);
      expect(deletedSessionRecord, equals(sessionRecord));
      final SessionRecord? notExistingSessionRecord = await adapter.fetchRecord(deletedSessionRecord!.id);
      expect(notExistingSessionRecord, isNull);

      await adapter.storeLocalIdentity(identity);
      final fetchedIdentity = await adapter.fetchLocalIdentity();
      expect(fetchedIdentity, equals(identity));
      final deletedIdentity = await adapter.deleteLocalIdentity();
      expect(deletedIdentity, equals(fetchedIdentity));
      final notExistingLocalIdentity = await adapter.fetchLocalIdentity();
      expect(notExistingLocalIdentity, isNull);
    });
  });
}
