import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/flutter_black_sun_test.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('X3DH', () {
    test('should execute correctly', () async {
      // In this scenario, we have two parties Alice and Bob that want to
      // establish a shared key through an (unsecure) server.

      // X3DH requires a server to be accessible by Alice and Bob. Access to
      // this server is provided by KeyManager service, whose
      // mock implementation MockKeyManager is what we use in
      // this test.

      // This test is performed from Alice' perspective, making Bob the
      // remote party.

      // Create an identity for Alice and Bob.
      final aliceId = Identity.createNew(
        User(address: "alice@test.com"),
        "test.example.com",
      );
      final bobId = Identity.createNew(
        User(address: "bob@test.com"),
        "test.example.com",
      );

      // Alice and Bob get their own mocked services to interact with.
      final keyManagerAlice = MockKeyManager();
      final envAlice = Environment(
        keyManager: keyManagerAlice,
        deviceStateDatabaseAdapter: MockDeviceStateDatabaseAdapter(),
        keyProvider: DefaultKeyProvider(),
        keyStore: MockKeyStore(),
        messageService: MockMessagingService(keyManager: keyManagerAlice),
      );

      final keyManagerBob = MockKeyManager();
      final envBob = Environment(
        keyManager: keyManagerBob,
        deviceStateDatabaseAdapter: MockDeviceStateDatabaseAdapter(),
        keyProvider: DefaultKeyProvider(),
        keyStore: MockKeyStore(),
        messageService: MockMessagingService(keyManager: keyManagerBob),
      );

      // We now use the Sesame facade for setting up cyptographic data
      // required for performing X3DH in each simulated enviroment.
      final sesameAlice = BlackSun(environment: envAlice);
      await sesameAlice.setupLocalDevice(aliceId);
      final sesameBob = BlackSun(environment: envBob);
      await sesameBob.setupLocalDevice(bobId);

      // Now create the X3DH protocol endpoints for Alice and Bob, respectively.
      final x3dhAlice = envAlice.x3dh;
      final x3dhBob = envBob.x3dh;

      // This concludes the setup. We are now ready to have Alice execute the
      // X3DH protocol with Bob, which eventually will provide with a shared key
      // and some other data she will require in post-X3DH protocols, such as
      // Double Ratchet.
      final x3dhOutput = await x3dhAlice.initiateProtocol(aliceId, bobId);

      // In order to test the correctness, we simulate an initial (empty)
      // message sent from Alice to Bob. In the end, both Alice and and Bob
      // should have obtained the same shared key and associated data.

      // Bob bob completes the X3DH protocol on his side. From his perspective,
      // Alice is the initiating identity.
      final bobParams = await x3dhBob.completeProtocol(
        bobId,
        aliceId,
        x3dhOutput.publicParameters,
      );

      // Now we make the final assertions.
      expect(bobParams.associatedData, x3dhOutput.privateParameters.associatedData);
      expect(bobParams.sharedKey, x3dhOutput.privateParameters.sharedKey);
    });
  });
}
