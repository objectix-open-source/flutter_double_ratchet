import 'dart:typed_data';

import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/device_record.dart';
import 'package:flutter_black_sun/src/device-state/device_state_store.dart';
import 'package:flutter_black_sun/src/device-state/session_record.dart';
import 'package:flutter_black_sun/src/device-state/user_record.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet_session_state.dart';
import 'package:flutter_black_sun/src/services/mocks/mock_device_state_database_adapter.dart';
import 'package:flutter_black_sun/src/util/uuid_shortener.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:uuid/uuid.dart';

void main() {
  group("Device State Store", () {
    final adapter = MockDeviceStateDatabaseAdapter();
    final store = DeviceStateStore(persistenceAdapter: adapter);

    UserRecord? userRecord;
    DeviceRecord? deviceRecord;
    SessionRecord? sessionRecord;

    Future<DeviceRecord> createNewDeviceRecord() async {
      final identityKey = await DefaultKeyProvider().generateKey();

      final result = DeviceRecord(
        userRecordId: userRecord!.id,
        identity: Identity.createNew(userRecord!.user, "de.objectix.de"),
        identityKey: await identityKey.extractPublicKey(),
      );

      userRecord!.deviceRecords.addTail(result);
      return result;
    }

    SessionRecord createNewSessionRecord(DeviceRecord devRec) {
      final associatedData = "Some associated data".codeUnits;
      final result = SessionRecord(
        id: const Uuid().v4short(),
        deviceRecordId: devRec.id,
        state: DoubleRatchetSessionState(),
        associatedData: Uint8List.fromList(
          associatedData,
        ),
      );
      devRec.sessionRecords.addTail(result);
      return result;
    }

    setUp(() async {
      await store.reset();
      await adapter.reset();

      final user = User(address: "test@example.com");
      final identityKey = await DefaultKeyProvider().generateKey();
      final identity = Identity.createNew(
        user,
        "de.objectix.test",
      );

      userRecord = UserRecord(
        user: user,
      );

      // Add a device record to the user record
      deviceRecord = DeviceRecord(
        userRecordId: userRecord!.id,
        identity: identity,
        identityKey: await identityKey.extractPublicKey(),
      );
      userRecord!.deviceRecords.addTail(deviceRecord!);

      // Add session record to device record
      final associatedData = "Some associated data".codeUnits;
      sessionRecord = SessionRecord(
        id: const Uuid().v4short(),
        deviceRecordId: identity.id,
        state: DoubleRatchetSessionState(),
        associatedData: Uint8List.fromList(associatedData),
      );
      deviceRecord!.sessionRecords.addTail(sessionRecord!);

      // Write store to persistence adapter
      await store.storeUserRecord(userRecord!);
      await store.commit();
    });

    test("should write UserRecords and associated entities correctly", () async {
      // Force refetch from persistence adapter.
      await store.reset();

      final userShouldExist = await adapter.fetchRecord(userRecord!.id);
      final deviceShouldExist = await adapter.fetchRecord(deviceRecord!.id);
      final sessionShouldExist = await adapter.fetchRecord(sessionRecord!.id);
      expect(userShouldExist, equals(userRecord));
      expect(deviceShouldExist, equals(deviceRecord));
      expect(sessionShouldExist, equals(sessionRecord));
    });

    test("should delete DeviceRecords and associated entities correctly", () async {
      // Add another device and session to the user record.
      final anotherDevice = await createNewDeviceRecord();
      final anotherSessionOnAnotherDevice = createNewSessionRecord(anotherDevice);
      await store.storeUserRecord(userRecord!);
      await store.commit();

      // Device should be member of the user record's device sessions, and
      // new device should contain the new session, too.
      expect(userRecord!.deviceRecords.isMember(anotherDevice.id), isTrue);
      expect(
        anotherDevice.sessionRecords.isMember(
          anotherSessionOnAnotherDevice.id,
        ),
        isTrue,
      );

      // The second device should be written to store, too.
      final DeviceRecord? writtenDevice = await adapter.fetchRecord(anotherDevice.id);
      expect(writtenDevice, isNotNull);

      // Now delete this second device.
      final deletedDevice = await store.deleteDeviceRecord(anotherDevice.id);
      expect(deletedDevice, isNotNull);

      // Check that delete markers have been properly set.
      expect(deletedDevice!.isDeleted, isTrue);

      // The deleted device should not be a member of the user record anymore.
      expect(userRecord!.deviceRecords.isMember(anotherDevice.id), isFalse);

      // If we now commit the change to the database, the deleted device
      // and associated session should not be there anymore.
      await store.commit();
      final DeviceRecord? deviceShouldNotExist = await adapter.fetchRecord(anotherDevice.id);
      expect(deviceShouldNotExist, isNull);
      final DeviceRecord? sessionShouldNotExist = await adapter.fetchRecord(anotherSessionOnAnotherDevice.id);
      expect(sessionShouldNotExist, isNull);
    });

    test("should delete SessionRecords correctly", () async {
      final anotherSession = createNewSessionRecord(deviceRecord!);
      await store.storeUserRecord(userRecord!);
      await store.commit();

      // Session should be member of the user record's device sessions, and
      // new device should contain the new session, too.
      expect(
        deviceRecord!.sessionRecords.isMember(
          anotherSession.id,
        ),
        isTrue,
      );

      // Now delete the second session.
      final deletedSession = await store.deleteSessionRecord(anotherSession.id);
      expect(deletedSession, isNotNull);

      // Check that delete markers have been properly set.
      expect(deletedSession!.isDeleted, isTrue);

      // The deleted session should not be a member of the device record
      // anymore.
      expect(deviceRecord!.sessionRecords.isMember(anotherSession.id), isFalse);

      // If we now commit the change to the database, the deleted device
      // and associated session should not be there anymore.
      await store.commit();
      final DeviceRecord? sessionShouldNotExist = await adapter.fetchRecord(anotherSession.id);
      expect(sessionShouldNotExist, isNull);
    });

    test("should perform rollback correctly", () async {
      // Make another change to the device record
      final updatedDevice = deviceRecord!.copyWith(identityKey: null);
      await store.storeDeviceRecord(updatedDevice);

      // And a final change to the session record
      final updatedSession = sessionRecord!.copyWith(associatedData: null);
      await store.storeSessionRecord(updatedSession);

      // We make a change by deleting our user record
      final deletedUserRecord = await store.deleteUserRecord(userRecord!.id);

      // Check that delete marker were set
      expect(deletedUserRecord, isNotNull);
      expect(deletedUserRecord!.isDeleted, isTrue);

      // Now, we perform a rollback
      await store.rollback();

      // Make sure that all changes have been discarded.
      final user = await store.fetchUserRecord(userRecord!.id);
      final device = await store.fetchDeviceRecord(deviceRecord!.id);
      final session = await store.fetchSessionRecord(sessionRecord!.id);

      // Deletion was undone
      expect(user, isNotNull);
      expect(device, isNotNull);
      expect(session, isNotNull);

      // Changes to object have been discarded, too?
      expect(device, equals(deviceRecord));
      expect(session, equals(sessionRecord));
    });
  });
}
