import 'dart:typed_data';

import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/flutter_black_sun_test.dart';
import 'package:flutter_black_sun/src/device-state/device_record.dart';
import 'package:flutter_black_sun/src/device-state/device_state_manager.dart';
import 'package:flutter_black_sun/src/device-state/device_state_store.dart';
import 'package:flutter_black_sun/src/device-state/session_record.dart';
import 'package:flutter_black_sun/src/device-state/user_record.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet_session_state.dart';
import 'package:flutter_black_sun/src/sesame/session_builder.dart';
import 'package:flutter_black_sun/src/util/uuid_shortener.dart';
import 'package:flutter_black_sun/src/x3dh/x3dh.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:uuid/uuid.dart';

void main() {
  group("Device State Manager", () {
    DeviceStateManager buildDeviceStateManager() {
      final keystore = MockKeyStore();
      final keyprovider = DefaultKeyProvider();
      final deviceStateStore = DeviceStateStore(
        persistenceAdapter: MockDeviceStateDatabaseAdapter(),
      );
      final keymanager = MockKeyManager();
      final x3dh = X3DH(
        keyStore: keystore,
        keyProvider: keyprovider,
        keyManager: keymanager,
      );
      final sessionBuilder = SessionBuilder(
        deviceStateStore: deviceStateStore,
        x3dh: x3dh,
        doubleRatchet: DoubleRatchet(),
        keyStore: keystore,
      );
      return DeviceStateManager(
        sessionBuilder: sessionBuilder,
        deviceStateStore: deviceStateStore,
      );
    }

    UserRecord? userRecord;
    DeviceRecord? deviceRecord;
    SessionRecord? sessionRecord;

    Future<void> fillDatabase(DeviceStateManager manager) async {
      final user = User(address: "test@example.com");
      final identityKey = await DefaultKeyProvider().generateKey();
      final identity = Identity.createNew(
        user,
        "de.objectix.test",
      );

      // Build a simple chain User > Device > Session.
      userRecord = UserRecord(
        user: user,
      );
      deviceRecord = DeviceRecord(
        userRecordId: userRecord!.id,
        identity: identity,
        identityKey: await identityKey.extractPublicKey(),
      );
      final associatedData = "Some associated data".codeUnits;
      sessionRecord = SessionRecord(
        id: const Uuid().v4short(),
        deviceRecordId: identity.id,
        state: DoubleRatchetSessionState(),
        associatedData: Uint8List.fromList(associatedData),
      );

      userRecord!.deviceRecords.addTail(deviceRecord!);
      deviceRecord!.sessionRecords.addTail(sessionRecord!);

      await manager.deviceStateStore.storeUserRecord(userRecord!);
      await manager.deviceStateStore.storeDeviceRecord(deviceRecord!);
      await manager.deviceStateStore.storeSessionRecord(sessionRecord!);
      await manager.deviceStateStore.commit();
    }

    test("should perform cascading deletes properly", () async {
      final manager = buildDeviceStateManager();
      await fillDatabase(manager);

      // If we delete the session record of such a simple chain, the manager
      // should also delete the device record and (in turn) the user record.
      await manager.deleteSession(sessionRecord!.id);
      await manager.deviceStateStore.commit();

      // Now directly check the adapter to see the objects in the database.
      final adapter = manager.deviceStateStore.persistenceAdapter;
      final UserRecord? fetchedUserRecord = await adapter.fetchRecord(userRecord!.id);
      expect(fetchedUserRecord, isNull);
      final DeviceRecord? fetchedDeviceRecord = await adapter.fetchRecord(deviceRecord!.id);
      expect(fetchedDeviceRecord, isNull);
      final SessionRecord? fetchedSessionRecord = await adapter.fetchRecord(sessionRecord!.id);
      expect(fetchedSessionRecord, isNull);
    });

    test("should insert and activate sessions correctly", () async {
      final manager = buildDeviceStateManager();
      await fillDatabase(manager);

      // Is the right session active?
      expect(deviceRecord!.activeSession, equals(sessionRecord!.id));

      // Create another session
      final anotherSession = SessionRecord(
        id: const Uuid().v4short(),
        deviceRecordId: deviceRecord!.id,
        state: DoubleRatchetSessionState(),
        associatedData: Uint8List(10),
      );

      await manager.insertSession(anotherSession);

      // Inserted session should be the active session now.
      expect(deviceRecord!.activeSession, equals(anotherSession.id));

      // We should have two sessions attached to device
      expect(deviceRecord!.sessionRecords.count, 2);

      // Re-activate the previous session
      await manager.activateSession(sessionRecord!.id);
      expect(deviceRecord!.activeSession, equals(sessionRecord!.id));
      expect(deviceRecord!.sessionRecords.count, 2);

      // Activating an already activated session is an error
      expectLater(
        manager.activateSession(sessionRecord!.id),
        throwsAssertionError,
      );
    });

    test("should correctly mark users and devices as stale", () async {
      final manager = buildDeviceStateManager();
      await fillDatabase(manager);

      await manager.markUserAsStale(userRecord!.id);
      await manager.markDeviceAsStale(deviceRecord!.id);

      final updatedUserRecord = await manager.deviceStateStore.fetchUserRecord(
        userRecord!.id,
      );
      expect(updatedUserRecord, isNotNull);
      expect(updatedUserRecord!.isStale, isTrue);
      final updatedDeviceRecord = await manager.deviceStateStore.fetchDeviceRecord(
        deviceRecord!.id,
      );
      expect(updatedDeviceRecord, isNotNull);
      expect(updatedDeviceRecord!.isStale, isTrue);
    });

    test("should correctly perform conditional updates", () async {
      final manager = buildDeviceStateManager();
      final user = User(address: "test@example.com");
      final identity = Identity.createNew(user, "de.objectix.test");
      final keypair = await DefaultKeyProvider().generateKey();
      final localIdentity = Identity.createNew(
        User(
          address: "local@example.com",
        ),
        "de.objectix.test",
      );
      await manager.deviceStateStore.persistenceAdapter.storeLocalIdentity(
        localIdentity,
      );

      await manager.conditionalUpdate(
        identity,
        await keypair.extractPublicKey(),
      );

      // The store should now have create a user with a single device attached
      // to it.
      final newUserRecord = await manager.deviceStateStore.fetchUserRecord(
        user.address,
      );
      expect(newUserRecord, isNotNull);
      final newDeviceRecord = await manager.deviceStateStore.fetchDeviceRecord(
        identity.id,
      );
      expect(newDeviceRecord, isNotNull);
      expect(
        newUserRecord!.deviceRecords.isMember(newDeviceRecord!.id),
        isTrue,
      );
      expect(newDeviceRecord.sessionRecords.isEmpty, isTrue);

      // Now use another identity key
      final newKeypair = await DefaultKeyProvider().generateKey();
      final newPublicKey = await newKeypair.extractPublicKey();

      await manager.conditionalUpdate(identity, newPublicKey);

      // Refetch records
      final refetchedUserRecord = await manager.deviceStateStore.fetchUserRecord(
        user.address,
      );
      expect(refetchedUserRecord, isNotNull);
      expect(refetchedUserRecord!.deviceRecords.count, 1);

      final refetchedDeviceRecord = await manager.deviceStateStore.fetchDeviceRecord(
        identity.id,
      );
      expect(refetchedDeviceRecord, isNotNull);
      expect(
        refetchedUserRecord.deviceRecords.isMember(refetchedDeviceRecord!.id),
        isTrue,
      );
      expect(refetchedDeviceRecord.sessionRecords.isEmpty, isTrue);
      expect(refetchedDeviceRecord.identityKey, equals(newPublicKey));
    });

    test("should correctly prepare for encryption", () async {
      // We need to setup two different identities Alice and Bob in order to prepare for encryption.
      // In this test, we use Alice as the local identity, which prepares for encryption against Bob's device.
      final keyManagerAlice = MockKeyManager();
      final aliceEnv = Environment(
        keyManager: keyManagerAlice,
        deviceStateDatabaseAdapter: MockDeviceStateDatabaseAdapter(),
        keyProvider: DefaultKeyProvider(),
        keyStore: MockKeyStore(),
        messageService: MockMessagingService(keyManager: keyManagerAlice),
      );
      final alice = User(address: "alice@example.com");
      final identityAlice = Identity.createNew(alice, "de.objectix.test");
      final sesameAlice = BlackSun(environment: aliceEnv);
      await sesameAlice.setupLocalDevice(identityAlice);

      final keyManagerBob = MockKeyManager();
      final bobEnv = Environment(
        keyManager: keyManagerBob,
        deviceStateDatabaseAdapter: MockDeviceStateDatabaseAdapter(),
        keyProvider: DefaultKeyProvider(),
        keyStore: MockKeyStore(),
        messageService: MockMessagingService(keyManager: keyManagerBob),
      );
      final bob = User(address: "bob@example.com");
      final identityBob = Identity.createNew(bob, "de.objectix.test");
      final sesameBob = BlackSun(environment: bobEnv);
      await sesameBob.setupLocalDevice(identityBob);

      final idKeyBob = await aliceEnv.keyManager.fetchIdentityKey(identityBob);
      await aliceEnv.deviceStateManager.prepareForEncryption(identityBob, idKeyBob);

      // Bob's device record should now have an active session.
      final remoteDevice = await aliceEnv.deviceStateStore.fetchDeviceRecord(identityBob.id);
      expect(remoteDevice, isNotNull);
      expect(remoteDevice!.activeSession, isNotNull);

      // Initialized session should exist
      final session = await aliceEnv.deviceStateStore.fetchSessionRecord(remoteDevice.activeSession!);
      expect(session, isNotNull);
      expect(session!.deviceRecordId, equals(remoteDevice.id));
    });
  });
}
