import 'dart:typed_data';

import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/flutter_black_sun_test.dart';
import 'package:flutter_test/flutter_test.dart';

Environment setupMockEnvirommentForIdentity(Identity identity) {
  final keyManager = MockKeyManager();
  keyManager.reset();

  final messageService = MockMessagingService(keyManager: keyManager);
  messageService.reset();

  return Environment(
    deviceStateDatabaseAdapter: MockDeviceStateDatabaseAdapter(),
    keyManager: keyManager,
    keyProvider: DefaultKeyProvider(),
    keyStore: MockKeyStore(),
    messageService: messageService,
  );
}

void main() {
  group('BlackSun', () {
    test('should setup correctly', () async {
      // In this scenario, we have a new device, which needs to be setup
      // before being able to use Sesame in a regular manner.

      // We use Alice as our local device' user identity.
      final localIdentity = Identity.createNew(
        User(address: "alice@example.com"),
        "test.example.com",
      );

      // Setup a mocked Sesame Environment.
      final sesameEnvironment = setupMockEnvirommentForIdentity(localIdentity);

      // We use Sesame's setup function to accomplish device setup with respect
      // to Sesame initialization.
      final sesame = BlackSun(environment: sesameEnvironment);

      await sesame.setupLocalDevice(localIdentity);

      // Assert that all required data has been created during setup.
      final keyStore = sesameEnvironment.keyStore;
      final stateStore = sesameEnvironment.deviceStateDatabaseAdapter;

      // Identity key must be set
      final idKey = await keyStore.fetchIdentityKey();
      expect(idKey, isNotNull);

      // Signing key must be set
      final signKey = await keyStore.fetchSigningKey();
      expect(signKey, isNotNull);

      // Prekey must be set and active
      final preKey = await keyStore.fetchActiveSignedPrekey();
      expect(preKey, isNotNull);

      // A set of one-time keys must have been created
      final otk = await keyStore.fetchOnetimePrekeys();
      expect(otk, isNotEmpty);
      expect(otk, hasLength(sesame.onetimePrekeyBatchSize));

      // Local identity must be set correctly
      final setupLocalIdentity = await stateStore.fetchLocalIdentity();
      expect(setupLocalIdentity, isNotNull);
      expect(setupLocalIdentity!, equals(localIdentity));

      // A valid keypackage must have been published to Key server.
      final keypackage = await sesameEnvironment.keyManager.fetchKeyPackage(localIdentity);
      expect(keypackage.identity, equals(localIdentity));
      expect(
        keypackage.identityKey,
        equals(
          await idKey!.extractPublicKey(),
        ),
      );
      final otkPubkeysLocal = await Future.wait(
        otk.map(
          (k) async => await k.keyPair.extractPublicKey(),
        ),
      );
      final otkPubkeysKeypackage = keypackage.onetimePrekeys.map(
        (e) => e.key,
      );
      expect(otkPubkeysKeypackage, containsAll(otkPubkeysLocal));
      expect(otkPubkeysLocal, containsAll(otkPubkeysKeypackage));
      expect(
        keypackage.signedPrekey.prekey.key,
        equals(
          await preKey!.prekey.keyPair.extractPublicKey(),
        ),
      );
      expect(keypackage.signedPrekey.signature, preKey.signature);
    });

    test('should exchange data correctly between two identities', () async {
      // In this scenario we setup two parties Alice and Bob. We let Alice
      // send an encrypted message M, resulting in encrypted message M' that
      // is sent to Bob's mailbox. Bob then fetches and decrypts M' to retrieve
      // M. In a second step we let Bob send back another Message N to Alice, to
      // assert bi-directional communication functionality.

      // Because the setupMockEnvironment() function resets some mocks, we
      // need to call this fucntion for all test identities, before advancing
      // with any other setup.
      final identityAlice = Identity.createNew(
        User(address: "alice@example.com"),
        "test.example.com",
      );
      final identityBob = Identity.createNew(
        User(address: "bob@example.com"),
        "test.example.com",
      );

      final environmentAlice = setupMockEnvirommentForIdentity(identityAlice);
      final environmentBob = setupMockEnvirommentForIdentity(identityBob);

      // Start with setting up Alice' device.
      final alice = BlackSun(environment: environmentAlice);
      await alice.setupLocalDevice(identityAlice);
      final inboundMessagesAlice = await alice.connectToMailbox();

      // Then setup Bob's device.
      final bob = BlackSun(environment: environmentBob);
      await bob.setupLocalDevice(identityBob);
      final inboundMessagesBob = await bob.connectToMailbox();

      // Because Alice want to initiate a message to Bob, we need to
      // add Bob and his devices to Alice' device and prepare them for
      // encryption.
      await alice.addCorrespondent(identityBob.user);

      // Alice constructs test message M.
      List<int> codeUnitsM = "Hello Bob".codeUnits;
      final Uint8List messageM = Uint8List.fromList(codeUnitsM);

      // Send message M to Bob
      await alice.send(messageM, [identityBob.user]);

      // Assert that Bob receives the correct message.
      inboundMessagesBob.listen(
        expectAsync1(
          (InboundMessage messageReceivedByBob) async {
            // A message was received. Assert that this message is identical
            // to the original message M sent by Alice.
            expect(messageReceivedByBob.plaintext, equals(messageM));
            // Sender data should be correct
            expect(messageReceivedByBob.sender, equals(identityAlice));
          },
        ),
      );

      // Wait for a message to arrive, before Bob sends back a response.
      await inboundMessagesBob.isEmpty;

      // Create a new test message N.
      List<int> codeUnitsN = "Hello Alice".codeUnits;
      final Uint8List messageN = Uint8List.fromList(codeUnitsN);

      // Send message N back to Alice
      await bob.send(messageN, [identityAlice.user]);

      // Assert that Alice received the correct message.
      inboundMessagesAlice.listen(
        expectAsync1(
          (InboundMessage messageReceivedByAlice) {
            // Assert that receive message equals Bob's reponse message N.
            expect(messageReceivedByAlice.plaintext, equals(messageN));
            // Sender data should be correct
            expect(messageReceivedByAlice.sender, equals(identityBob));
          },
        ),
      );
    });
  });
}
