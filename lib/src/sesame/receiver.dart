import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/device_record.dart';
import 'package:flutter_black_sun/src/device-state/session_record.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet_message.dart';
import 'package:flutter_black_sun/src/exceptions/exceptions.dart';
import 'package:flutter_black_sun/src/util/data_input_stream.dart';

/// Implements the Sesame receiving process as specified in Section 3.4 of the
/// Sesame specification at https://signal.org/docs/specifications/sesame/.
extension Receiver on BlackSun {
  /// Reads and processes inbound encrypted messages from the message server and
  /// forwards the resulting decrypted messages via the returned stream.
  Future<Stream<InboundMessage>> connectToMailbox() async {
    // Fetch local identity
    final localIdentity = await environment.deviceStateDatabaseAdapter.fetchLocalIdentity();
    assert(localIdentity != null);

    final serverStream = await environment.messageService.connect(localIdentity!);

    return serverStream
        .asyncMap((message) async {
          try {
            final msg = await _receive(message);
            // A this point, receive was successful -> commit device state.
            await environment.deviceStateStore.commit();
            return msg;
          } catch (ex) {
            // Receive failed -> discard changes made to device state during
            // rollback. Discard received message.
            await environment.deviceStateStore.rollback();
            return null;
          }
        })
        .where((msg) => msg != null)
        .map((msg) => msg!);
  }

  Future<InboundMessage> _receive(MessageServiceInboundMessage message) async {
    final inputStream = DataInputStream(buffer: message.payload);

    // Read initial message flag
    final isInitialMessage = inputStream.readByte();

    // Attempt to read device record for the sender's device
    DeviceRecord? senderDevice = await environment.deviceStateStore.fetchDeviceRecord(
      message.fromIdentity.id,
    );

    DoubleRatchetMessage ciphertext;
    SessionRecord? relevantSession;

    if (isInitialMessage == 0x01) {
      // If the encrypted message is an initiation message and the recipient
      // device does not have a relevant DeviceRecord containing a session that
      // can decrypt the message, then the following steps are performed.

      // Read sender identity key
      final senderIdentityKeyBytes = inputStream.readBytes();
      final senderIdentityKey = SimplePublicKey(
        senderIdentityKeyBytes,
        type: KeyPairType.x25519,
      );
      // Ephemeral key
      final ephemeralKeyBytes = inputStream.readBytes();
      final ephemeralKey = SimplePublicKey(
        ephemeralKeyBytes,
        type: KeyPairType.x25519,
      );
      // Prekey ID used by sender
      final prekeyId = inputStream.readString();
      // One-time prekey used by sender
      final otkId = inputStream.readString();

      // Read ciphertext.
      ciphertext = DoubleRatchetMessage.fromInputStream(inputStream);

      // If we have a device, does that device have a relevant session, that
      // can decrypt the message?
      if (senderDevice != null) {
        relevantSession = await _findRelevantSession(senderDevice, ciphertext);
      }

      if (senderDevice == null || relevantSession == null) {
        // Either no device or no relevant session found yet.

        // We perform a conditional update on the device state. Then we create
        // a new session, and insert it into our device record.
        await environment.deviceStateManager.conditionalUpdate(
          message.fromIdentity,
          senderIdentityKey,
        );

        // Re-fetch device record
        senderDevice = await environment.deviceStateStore.fetchDeviceRecord(
          message.fromIdentity.id,
        );

        assert(senderDevice != null);

        // Create a new session from our initial X3DH data
        final newSession = await environment.sessionBuilder.newDeviceSessionFromInitialData(
          senderDevice!.identity.id,
          senderDevice.identity,
          senderIdentityKey,
          ephemeralKey,
          prekeyId,
          otkId,
        );

        await environment.deviceStateManager.insertSession(newSession);
        relevantSession = newSession;
      }
    } else {
      // No initial message. Just read ciphertext now.
      ciphertext = DoubleRatchetMessage.fromInputStream(inputStream);

      if (senderDevice != null) {
        // Attempt to find a relevant session in the sedner device.
        relevantSession = await _findRelevantSession(
          senderDevice,
          ciphertext,
        );
      }
    }

    assert(senderDevice != null);

    // If no session in the relevant DeviceRecord can decrypt the encrypted
    // message, then the encrypted message is discarded, all changes to device
    // state are discarded, and this process terminates.
    if (relevantSession == null) {
      throw const InvalidMessageException();
    }

    // Decrypt the ciphertext using the relevant session.
    final plaintext = await environment.doubleRatchet.decrypt(
      relevantSession.state,
      ciphertext,
      relevantSession.associatedData,
    );

    // If the relevant session is not active it is activated.
    if (senderDevice!.activeSession != relevantSession.id) {
      await environment.deviceStateManager.activateSession(relevantSession.id);
    }

    // If the relevant session was not initialized yet, we can mark it as initialized now.
    if (!relevantSession.isInitialized) {
      final updatedSession = relevantSession.copyWith(isInitialized: true);
      await environment.deviceStateStore.storeSessionRecord(updatedSession);
    }

    return InboundMessage(
      plaintext: plaintext,
      sender: message.fromIdentity,
      receivedAt: DateTime.now().millisecondsSinceEpoch,
    );
  }

  Future<SessionRecord?> _findRelevantSession(
    DeviceRecord deviceRecord,
    DoubleRatchetMessage ciphertext,
  ) async {
    // Iterate through all session and find the one that can decrypt the
    // ciphertext.
    for (final session in deviceRecord.sessionRecords.entities) {
      try {
        await environment.doubleRatchet.decrypt(
          session.state.copy(), // Fill in a copy of our original state
          ciphertext,
          session.associatedData,
        );

        // At this point, the session was able to decrypt the ciphertext.
        return session;
      } catch (ex) {
        continue;
      }
    }

    return null;
  }
}
