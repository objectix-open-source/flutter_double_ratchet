import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/user_record.dart';

extension Setup on BlackSun {
  /// Setup function for local device. Call this function in order to setup
  /// the local device for use with Sesame. Unless the local device has not
  /// been successfully setup, you need to call this function first, before
  /// successfully calling any other method of this facade.
  Future<void> setupLocalDevice(Identity identity) async {
    // (1) Create a fresh identity key for the local device.
    final identityKey = await environment.keyProvider.generateKey();

    // (2) Create a signing key for the local device.
    final signingKey = await environment.keyProvider.generateSigningKey();

    // (3) Create a signed prekey for the local device.
    final signedPrekey = await environment.keyProvider.generateSignedPrekey(signingKey);

    // (4) Create a sufficient number of onetime prekeys.
    final List<Prekey> onetimeKeyBatch = [];
    for (var i = 0; i < onetimePrekeyBatchSize; i++) {
      final onetimePrekey = await environment.keyProvider.generatePrekey();
      onetimeKeyBatch.add(onetimePrekey);
    }

    // At this point we have created all crpytographic data that is required
    // for Sesame algorithm. We now need to store those in our stores.

    // (5) Store local identity key in key store
    await environment.keyStore.storeIdentityKey(identityKey);

    // (6) Store signing key
    await environment.keyStore.storeSigningKey(signingKey);

    // (7) Store signed prekey and activate it.
    await environment.keyStore.storeSignedPrekey(signedPrekey);
    await environment.keyStore.activateSignedPrekey(signedPrekey.fingerprint);

    // (8) Store the generated onetime prekeys
    await environment.keyStore.storeOneTimePrekeys(onetimeKeyBatch);

    // (9) Write the user record for our local identity.
    final userRecord = UserRecord(user: identity.user);
    await environment.deviceStateDatabaseAdapter.storeRecord(userRecord);
    await environment.deviceStateDatabaseAdapter.storeLocalIdentity(identity);

    // At this point the generated cryptographic material has been stored
    // on our local device. Next, we need to publish the public parts of
    // that material on the Key server.

    // Extract the public keys from our onetime prekeys
    final List<PublicPrekey> publicOnetimePrekeys = await Future.wait(
      onetimeKeyBatch.map(
        (k) async => await k.public(),
      ),
    );

    // Build key package struct that gets published to the Key server.
    final KeyPackage keyPackage = KeyPackage(
      identity: identity,
      identityKey: await identityKey.extractPublicKey(),
      signingKey: await signingKey.extractPublicKey(),
      signedPrekey: await signedPrekey.public(),
      onetimePrekeys: publicOnetimePrekeys,
    );

    // Publish our key package to the Key server
    await environment.keyManager.publish(keyPackage);

    // Register our identity with the messaging server.
    await environment.messageService.register(identity);

    // Setup of our local device (as far as Sesame is concerned) is now
    // complete.
  }
}
