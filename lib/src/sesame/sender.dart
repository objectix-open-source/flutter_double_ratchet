import 'dart:typed_data';

import 'package:collection/collection.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/device_record.dart';
import 'package:flutter_black_sun/src/util/data_output_stream.dart';

/// Implements the Sesame sending process as specified in Section 3.3 of the
/// Sesame specification at https://signal.org/docs/specifications/sesame/.
extension Sender on BlackSun {
  /// Writes a plaintext message to the local Sesame system. This performs a
  /// cryptographically secure broadcast to the specified user identifiers and
  /// their associated devices.
  Future<void> send(Uint8List data, List<User> recipientUsers) async {
    await _send(data, recipientUsers);
  }

  Future<void> _send(Uint8List data, List<User> recipients) async {
    // Fetch local device user record and id
    final localIdentity = await environment.deviceStateStore.persistenceAdapter.fetchLocalIdentity();

    assert(localIdentity != null);

    for (var recipient in recipients) {
      try {
        await _dispatchForUser(recipient, data, localIdentity!);
        // At this point, message was successfully sent to user -> commit any changed made to the device state for that
        // user
        await environment.deviceStateStore.commit();
      } catch (ex) {
        // An error during dispatch occurred -> rollback session state to discard changes made for that
        // user, his/her devices and associated sessions.
        await environment.deviceStateStore.rollback();
      }
    }
  }

  // Performs a dispatch operation fo the given user recipient and data.
  Future<void> _dispatchForUser(
    User recipient,
    Uint8List data,
    Identity localIdentity,
  ) async {
    int dispatchRetriesLeft = maxNumberOfDispatchRetries;

    // Limit max number of dispatch retries for this suer as suggested in
    // Sesame sending specification.
    while (dispatchRetriesLeft-- > 0) {
      // Encrypt messages for each known device of the recipient.
      final messagesToSend = await _encryptForUser(recipient, data);

      // (2) The recipient UserID is sent to the server, along with the list of
      // encrypted messages and a corresponding list of DeviceIDs indicating the
      // recipient mailbox for each message. These lists will be empty if no
      // relevant active sessions exist.

      // Send the generated (encrypted) messages to the messaging service.
      final dispatchReponse = await environment.messageService.dispatch(
        messagesToSend.toList(growable: false),
        recipient,
        localIdentity,
      );

      // (3) If the recipient UserID is currently in-use and the sender's list
      // of DeviceIDs is current for the recipient UserID, then the server
      // accepts the messages and the messages are sent to the relevant
      // mailboxes. This process then terminates for the recipient UserID,
      // returning to step 1 for the next recipient UserID.

      // (4) Otherwise the server rejects the messages and either informs the
      // sending device if the recipient UserID does not exist; or informs the
      // sending device of the old DeviceIDs and new DeviceIDs needed to make the
      // sending device's records current, and the identity public keys
      // corresponding to any new DeviceIDs.
      final dispatchOk = await _processDispatchResponse(
        dispatchReponse,
        recipient,
      );

      if (dispatchOk) {
        break;
      }
    }
  }

  // Helper: Processes a dispatch response returned by server.
  Future<bool> _processDispatchResponse(
    MessageServerDispatchResponse dispatchResponse,
    User recipient,
  ) async {
    switch (dispatchResponse.result) {
      case MessageServiceDispatchResult.accepted:
        // Server accepted request. We are done with current recipient
        return true;
      case MessageServiceDispatchResult.userUnknown:
        // (5) If the server indicates that the recipient UserID does not exist,
        // then the sending device marks the relevant UserRecord (if any) as
        // stale. The sending device then terminates this process for the
        // recipient UserID, returning to step 1 for the next recipient UserID.
        await environment.deviceStateManager.markUserAsStale(recipient.address);
        return true;
      case MessageServiceDispatchResult.recipientDevicesNotCurrent:
        // (6) For each old DeviceID, the sending device marks the relevant
        // DeviceRecord as stale.
        for (final deviceId in dispatchResponse.obsoleteDevices) {
          await environment.deviceStateManager.markDeviceAsStale(deviceId.id);
        }

        // (7) For each new DeviceID, the sending device preps for encrypting to
        // the tuple (UserID, DeviceID, relevant public key).
        for (var i = 0; i < dispatchResponse.newDevices.length; i++) {
          await environment.deviceStateManager.prepareForEncryption(
            dispatchResponse.newDevices[i],
            dispatchResponse.newDevicePublicKeys[i],
          );
        }
        // (8) This process is restarted from step 1 for the CURRENT recipient
        // UserID.
        return false;
    }
  }

  // Helper: Encrypts data for all devices of recipient.
  Future<List<MessageServiceOutboundMessage>> _encryptForUser(
    User recipient,
    Uint8List data,
  ) async {
    final userRecord = await environment.deviceStateStore.fetchUserRecord(recipient.address);

    if (userRecord == null || userRecord.isStale) {
      // User unknown or stale. Skip user and continue with next user.
      return [];
    }

    // Attempt to encrypt message for as many devices of the current user as
    // possible.
    final encryptedMessages = await Future.wait(
      userRecord.deviceRecords.entities.map(
        (deviceRecord) async {
          // Skip device record if it is stale or does not have an active session.
          if (deviceRecord.isStale || deviceRecord.activeSession == null) {
            return null;
          }

          // Encrypt for device
          return await _encryptForDevice(deviceRecord, data);
        },
      ),
    );

    // Filter out null messages from result
    return encryptedMessages.whereNotNull().toList(growable: false);
  }

  // Helper: encrypts the specified plaintext data for the specified device,
  // resulting in a message we can send to the message service.
  Future<MessageServiceOutboundMessage> _encryptForDevice(
    DeviceRecord deviceRecord,
    Uint8List data,
  ) async {
    assert(deviceRecord.activeSession != null);
    final activeSession = deviceRecord.sessionRecords.entityById(
      deviceRecord.activeSession!,
    );
    assert(activeSession != null);

    // Encrypt the data using the active session.
    final encryptedMessage = await environment.doubleRatchet.encrypt(
      activeSession!.state,
      data,
      activeSession.associatedData,
    );

    final outputStream = DataOutputStream();

    // If the active session has X3DH initial message data set, we need to
    // include the relevant pieces in the message, prefixing the actual
    // encrypted message.
    final imd = activeSession.initialMessageData;
    if (imd != null) {
      // 0x01 indicates that initial message data in prefixing cipherptext
      outputStream.writeByte(0x01);
      // Sender identity key
      outputStream.writeBytes(
        Uint8List.fromList(imd.initiatingIdentityKey.bytes),
      );
      // Ephemeral key
      outputStream.writeBytes(
        Uint8List.fromList(imd.ephemeralKey.bytes),
      );
      // Prekey ID used by sender
      outputStream.writeString(imd.remotePrekeyId);
      // One-time prekey used by sender
      outputStream.writeString(imd.onetimePrekeyId ?? "");
    } else {
      // 0x00 indicates that no initial message data is prefixing the ciphertext
      outputStream.writeByte(0x00);
    }

    // Add encrypted message to binary payload stream
    encryptedMessage.toOutputStream(outputStream);

    // Embed encrypted message into message suitable for message service
    return MessageServiceOutboundMessage(
      toDeviceId: deviceRecord.identity.id,
      payload: outputStream.byteBuffer,
    );
  }
}
