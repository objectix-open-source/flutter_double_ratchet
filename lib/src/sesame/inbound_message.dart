import 'package:flutter/foundation.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';

/// Encapsulates a single data message arrived at and processed by the local
/// Sesame system.
class InboundMessage {
  /// Plaintext bytes of the message.
  final Uint8List plaintext;

  /// The message sender's user identity.
  final Identity sender;

  // UTC timestamp (milliseconds) this message was received on this device.
  final int receivedAt;

  InboundMessage({
    required this.plaintext,
    required this.sender,
    required this.receivedAt,
  });
}
