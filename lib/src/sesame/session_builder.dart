import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/device_record.dart';
import 'package:flutter_black_sun/src/device-state/device_state_store.dart';
import 'package:flutter_black_sun/src/device-state/session_record.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet_session_state.dart';
import 'package:flutter_black_sun/src/util/uuid_shortener.dart';
import 'package:flutter_black_sun/src/x3dh/x3dh.dart';
import 'package:uuid/uuid.dart';

class SessionBuilder {
  final DeviceStateStore deviceStateStore;
  final X3DH x3dh;
  final DoubleRatchet doubleRatchet;
  final KeyStore keyStore;

  SessionBuilder({
    required this.deviceStateStore,
    required this.x3dh,
    required this.doubleRatchet,
    required this.keyStore,
  });

  /// Creates a new session record to be used with the local device.
  Future<SessionRecord> initiateNewDeviceSession(
    DeviceRecord deviceRecord,
  ) async {
    // Create a fresh Double Ratchet State
    final state = DoubleRatchetSessionState();

    // Fetch local identity
    final localIdentity = await deviceStateStore.fetchLocalIdentity();
    assert(localIdentity != null);

    // Initiate X3DH protocol between the local identity, and the rmeote
    // identity.
    final output = await x3dh.initiateProtocol(
      localIdentity!,
      deviceRecord.identity,
    );

    // Initialize double ratchet state for our new session
    await doubleRatchet.initSessionAsLocal(
      state,
      output.privateParameters.sharedKey,
      output.publicParameters.remotePrekey.prekey.key,
    );

    final ad = Uint8List.fromList(output.privateParameters.associatedData);

    return SessionRecord(
      id: const Uuid().v4short(),
      deviceRecordId: deviceRecord.identity.id,
      state: state,
      associatedData: ad,
      initialMessageData: output.publicParameters,
    );
  }

  /// Creates a new session record from remote party's initial data.
  Future<SessionRecord> newDeviceSessionFromInitialData(
    String deviceId,
    Identity initiatingIdentity,
    SimplePublicKey senderIdentityKey,
    SimplePublicKey ephemeralKey,
    String prekeyId,
    String? onetimePrekeyId,
  ) async {
    // Fetch local identity
    final localIdentity = await deviceStateStore.fetchLocalIdentity();
    assert(localIdentity != null);

    // Fetch local identity key
    final idKey = await keyStore.fetchIdentityKey();
    assert(idKey != null);

    // Fetch designated prekey from local key store
    final signedPrekey = await keyStore.fetchSignedPrekey(prekeyId);
    assert(signedPrekey != null);

    final dhParams = X3DHPublicParameters(
      initiatingIdentityKey: senderIdentityKey,
      remoteIdentityKey: await idKey!.extractPublicKey(),
      ephemeralKey: ephemeralKey,
      remotePrekeyId: signedPrekey!.fingerprint,
      remotePrekey: await signedPrekey.public(),
      onetimePrekeyId: onetimePrekeyId,
    );

    final x3dhPrivateParams = await x3dh.completeProtocol(
      localIdentity!,
      initiatingIdentity,
      dhParams,
    );

    final state = DoubleRatchetSessionState();

    // Initialize double ratchet session state.
    await doubleRatchet.initSessionAsRemote(
      state,
      x3dhPrivateParams.sharedKey,
      signedPrekey.prekey.keyPair,
    );

    return SessionRecord(
      id: const Uuid().v4short(),
      deviceRecordId: deviceId,
      state: state,
      associatedData: Uint8List.fromList(x3dhPrivateParams.associatedData),
    );
  }
}
