import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/session_record.dart';
import 'package:flutter_black_sun/src/device-state/types.dart';

/// Encapsulates a Device Record for use on the local device.
class DeviceRecord implements DeviceStateEntity {
  @override
  String get id => identity.id;

  @override
  final bool isDeleted;

  /// The identity of this device record.
  final Identity identity;

  /// Application namespace this device belongs to.
  String get applicationNamespace => identity.applicationNamespace;

  /// Communication sessions associated with this device.
  final OrderedOneToManyRelation<SessionRecord> sessionRecords;

  /// First session in list of session is considered the active one.
  String? get activeSession => sessionRecords.isEmpty ? null : sessionRecords.first;

  /// Has this record been marked as stale?
  final bool isStale;

  /// Identity key for the corresponding device.
  final SimplePublicKey identityKey;

  /// User id this device record belongs to.
  final String userRecordId;

  DeviceRecord({
    required this.userRecordId,
    required this.identity,
    required this.identityKey,
    this.isDeleted = false,
    this.isStale = false,
    OrderedOneToManyRelation<SessionRecord>? sessionRecords,
  }) : sessionRecords = sessionRecords ?? OrderedOneToManyRelation();

  /// Returns a copy of this Device Record, with the specified field values replaced by
  /// the corresponding arguments.
  DeviceRecord copyWith({
    bool? isStale,
    bool? isDeleted,
    SimplePublicKey? identityKey,
    OrderedOneToManyRelation<SessionRecord>? sessionRecords,
  }) {
    return DeviceRecord(
      userRecordId: userRecordId,
      identity: identity,
      identityKey: identityKey ?? this.identityKey,
      isStale: isStale ?? this.isStale,
      isDeleted: isDeleted ?? this.isDeleted,
      sessionRecords: sessionRecords ?? this.sessionRecords,
    );
  }
}
