import 'package:flutter_black_sun/src/device-state/device_record.dart';
import 'package:flutter_black_sun/src/device-state/types.dart';
import 'package:flutter_black_sun/src/black_sun/user.dart';

/// Encapsulates a User Record for use on the local device.
class UserRecord implements DeviceStateEntity {
  @override
  String get id => user.address;

  @override
  final bool isDeleted;

  /// The user this user record is associated with.
  final User user;

  /// Has this record been marked as stale?
  final bool isStale;

  /// Relation for the device records associated with this user.
  final OrderedOneToManyRelation<DeviceRecord> deviceRecords;

  UserRecord({
    required this.user,
    this.isDeleted = false,
    this.isStale = false,
    OrderedOneToManyRelation<DeviceRecord>? deviceRecords,
  }) : deviceRecords = deviceRecords ?? OrderedOneToManyRelation();

  /// Creates and returns a copy of this user record, with fields replaced by values
  /// given as arguments to this function.
  UserRecord copyWith({
    bool? isStale,
    bool? isDeleted,
    OrderedOneToManyRelation<DeviceRecord>? deviceRecords,
  }) {
    return UserRecord(
      user: user,
      isDeleted: isDeleted ?? this.isDeleted,
      isStale: isStale ?? this.isStale,
      deviceRecords: deviceRecords ?? this.deviceRecords,
    );
  }
}
