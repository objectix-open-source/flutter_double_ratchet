import 'dart:collection';

/// Interface for entities within the device state store.
abstract class DeviceStateEntity {
  /// Unique identifier of this entity.
  String get id;

  /// Has this entity been marked for deletion?
  bool get isDeleted;
}

/// Implements an ordered one-to-many relation from one entity to another
/// entity.
class OrderedOneToManyRelation<T extends DeviceStateEntity> {
  /// Identifiers of the destination identifiers in a particular order.
  final List<String> ids;

  /// Adds the specified entity to this relation's tail.
  void addTail(T entity) {
    ids.add(entity.id);
    _identifierSet.add(entity.id);
    _entities[entity.id] = entity;
  }

  /// Removes the last entity from this relation.
  String? removeTail() {
    if (ids.isNotEmpty) {
      final removedId = ids.removeLast();
      _identifierSet.remove(removedId);
      _entities.remove(removedId);
      return removedId;
    } else {
      return null;
    }
  }

  /// Removes the specified entity from relation.
  T? removeById(String id) {
    final removed = ids.remove(id);
    if (!removed) {
      return null;
    }
    final element = _entities.remove(id);
    assert(element != null);
    final found = _identifierSet.remove(id);
    assert(found);
    return element;
  }

  /// Adds the specified entity to this relation's head.
  void addHead(T entity) {
    ids.insert(0, entity.id);
    _identifierSet.add(entity.id);
    _entities[entity.id] = entity;
  }

  /// Returns true, if the specified entity is member of this relation.
  bool isMember(String entityId) {
    return _identifierSet.contains(entityId);
  }

  /// Sets the resolved entities in this relation. Specified entities must
  /// match the current identifiers in this relation in terms of ids and
  /// quantity.
  void resolveWithEntities(List<T> entities) {
    assert(entities.length == ids.length);
    for (final entity in entities) {
      assert(_identifierSet.contains(entity.id));
      _entities[entity.id] = entity;
    }
  }

  /// Returns the number of entities in this relation.
  int get count => ids.length;

  /// Returns the resolved entities in this relation.
  List<T> get entities {
    return ids.map(
      (id) {
        final entity = _entities[id];
        assert(entity != null);
        return entity!;
      },
    ).toList(growable: false);
  }

  /// Returns a resolved entity by its identifier (if existing).
  T? entityById(String id) {
    return _entities[id];
  }

  /// Returns true, if this relation does not contain an entity.
  bool get isEmpty => ids.isEmpty;

  /// Returns the first entity in this relation, if any.
  String? get first => isEmpty ? null : ids.first;

  OrderedOneToManyRelation({
    List<String>? ids,
  })  : ids = ids ?? [],
        _identifierSet = HashSet.from(ids ?? []),
        _entities = HashMap();

  // For quicker lookup, we keep track of an internal set of identifiers.
  final Set<String> _identifierSet;

  // The resolved entities. May be empty, if entities have not been resolved yet.
  final Map<String, T> _entities;
}
