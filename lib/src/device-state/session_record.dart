import 'dart:typed_data';

import 'package:flutter_black_sun/src/device-state/types.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet_session_state.dart';
import 'package:flutter_black_sun/src/x3dh/x3dh.dart';

/// Wraps a communication session this device is involved in. Such a session
/// is logically linked to exactly one other corresponding session on another
/// device.
class SessionRecord implements DeviceStateEntity {
  /// Globally unique identifier of this session.
  @override
  final String id;

  /// has sthis session been marked for deletion?
  @override
  final bool isDeleted;

  /// Device record id this session belongs to.
  final String deviceRecordId;

  /// Double Ratchet session state that is associated with this device session.
  final DoubleRatchetSessionState state;

  /// X3DH associated data to used with Double Ratchet encryption/decryption.
  final Uint8List associatedData;

  /// X3DH initial message data
  final X3DHPublicParameters? initialMessageData;

  /// If set to true, this session is treated as a session, that has been initialized.
  final bool isInitialized;

  const SessionRecord({
    required this.id,
    required this.deviceRecordId,
    required this.state,
    required this.associatedData,
    this.isDeleted = false,
    this.initialMessageData,
    this.isInitialized = false,
  });

  /// Creates and returns a copy of this session record, with fields replaced by values
  /// given as arguments to this function.
  SessionRecord copyWith({
    bool? isDeleted,
    DoubleRatchetSessionState? state,
    Uint8List? associatedData,
    bool? isInitialized,
  }) {
    return SessionRecord(
      id: id,
      isDeleted: isDeleted ?? this.isDeleted,
      deviceRecordId: deviceRecordId,
      state: state ?? this.state,
      associatedData: associatedData ?? this.associatedData,
      initialMessageData: (isInitialized ?? this.isInitialized) ? null : initialMessageData,
      isInitialized: isInitialized ?? this.isInitialized,
    );
  }
}
