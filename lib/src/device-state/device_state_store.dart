import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/device_record.dart';
import 'package:flutter_black_sun/src/device-state/session_record.dart';
import 'package:flutter_black_sun/src/device-state/types.dart';
import 'package:flutter_black_sun/src/device-state/user_record.dart';
import 'package:flutter_black_sun/src/services/device_state_database_adapter.dart';

/// Implements object access to an underlying device state database.
class DeviceStateStore {
  /// Persistence adapter to use with this store.
  final DeviceStateDatabaseAdapter persistenceAdapter;

  DeviceStateStore({
    required this.persistenceAdapter,
  });

  /// Resets the database context for this store, forcing subsequent operations
  /// on this store instance to re-fetch entities from database if required.
  Future<void> reset() async {
    _entities.clear();
  }

  /// Fetches the user record for the user, whose id is given by the argument passed on
  /// to this function.
  Future<UserRecord?> fetchUserRecord(String id) async {
    UserRecord? userRecord = await _fetch(id);
    if (userRecord != null) {
      final deviceRecords = await Future.wait(
        userRecord.deviceRecords.ids.map(
          (did) async {
            final deviceRecord = await fetchDeviceRecord(did);
            assert(deviceRecord != null);
            return deviceRecord!;
          },
        ),
      );
      userRecord.deviceRecords.resolveWithEntities(deviceRecords);
    }
    return userRecord;
  }

  /// Fetches the device record of the device, whose id is given bey the argument passed on
  /// to this function.
  Future<DeviceRecord?> fetchDeviceRecord(String id) async {
    DeviceRecord? deviceRecord = await _fetch(id);
    if (deviceRecord != null) {
      final sessionRecords = await Future.wait(
        deviceRecord.sessionRecords.ids.map(
          (sid) async {
            final sessionRecord = await fetchSessionRecord(sid);
            assert(sessionRecord != null);
            return sessionRecord!;
          },
        ),
      );
      deviceRecord.sessionRecords.resolveWithEntities(sessionRecords);
    }
    return deviceRecord;
  }

  /// Retrieves the session record for the device session, whose id is given by
  /// argument.
  Future<SessionRecord?> fetchSessionRecord(String id) async {
    return await _fetch(id);
  }

  /// Returns the local identity.
  Future<Identity?> fetchLocalIdentity() {
    return persistenceAdapter.fetchLocalIdentity();
  }

  /// Stores the specified user record, potentially replacing an already existing
  /// user record with the same id.
  Future<void> storeUserRecord(UserRecord record) async {
    // Store associated device records
    for (final deviceRecord in record.deviceRecords.entities) {
      await storeDeviceRecord(deviceRecord);
    }
    _entities[record.id] = record;
  }

  /// Stores the specified device record, potentially replacing an already existing
  /// device record with the same id.
  Future<void> storeDeviceRecord(DeviceRecord record) async {
    // Store associated session records
    for (final sessionRecord in record.sessionRecords.entities) {
      await storeSessionRecord(sessionRecord);
    }
    _entities[record.id] = record;
  }

  /// Stores the specified session record, potentially replacing an already existing
  /// session record with the same id.
  Future<void> storeSessionRecord(SessionRecord record) async {
    _entities[record.id] = record;
  }

  /// Deletes the user record given by its id. This also deletes any associated device
  /// records and session records, too.
  Future<UserRecord?> deleteUserRecord(String id) async {
    final UserRecord? userRecord = await fetchUserRecord(id);
    if (userRecord != null) {
      for (final did in List.from(userRecord.deviceRecords.ids)) {
        await deleteDeviceRecord(did);
      }
      final updatedRecord = userRecord.copyWith(isDeleted: true);
      _entities[id] = updatedRecord;
      return updatedRecord;
    } else {
      return null;
    }
  }

  /// Deletes the device record given by its id. This also deletes any associated
  /// session records, too.
  Future<DeviceRecord?> deleteDeviceRecord(String id) async {
    final DeviceRecord? deviceRecord = await fetchDeviceRecord(id);
    if (deviceRecord != null) {
      for (final did in List.from(deviceRecord.sessionRecords.ids)) {
        await deleteSessionRecord(did);
      }
      final updatedRecord = deviceRecord.copyWith(isDeleted: true);
      final owningUserRecord = await fetchUserRecord(updatedRecord.userRecordId);
      assert(owningUserRecord != null);
      owningUserRecord!.deviceRecords.removeById(updatedRecord.id);
      _entities[id] = updatedRecord;
      return updatedRecord;
    } else {
      return null;
    }
  }

  /// Deletes the session record given by its id.
  Future<SessionRecord?> deleteSessionRecord(String id) async {
    final SessionRecord? sessionRecord = await fetchSessionRecord(id);
    if (sessionRecord != null) {
      final updatedRecord = sessionRecord.copyWith(isDeleted: true);
      final owningDeviceRecord = await fetchDeviceRecord(updatedRecord.deviceRecordId);
      assert(owningDeviceRecord != null);
      owningDeviceRecord!.sessionRecords.removeById(updatedRecord.id);
      _entities[id] = updatedRecord;
      return updatedRecord;
    } else {
      return null;
    }
  }

  /// Discards any changes made since last commit.
  Future<void> rollback() async {
    // For now, we perform the rollback by just cleaning our db context.
    // This forces a re-fetch of requested entities from the database.
    _entities.clear();
  }

  /// Commits any changes made since last commit into database.
  Future<void> commit() async {
    // Write back all (non-deleted) entities into database.
    for (final entity in _entities.values.where((e) => !e.isDeleted)) {
      await persistenceAdapter.storeRecord(entity);
    }

    // Delete all deleted enities from database and db context.
    final deletedEntities = _entities.values.where((e) => e.isDeleted).toList(growable: false);
    for (final entity in deletedEntities) {
      await persistenceAdapter.deleteRecord(entity.id);
      _entities.remove(entity.id);
    }
  }

  // Helper that fetches entity from database and updates db context.
  Future<T?> _fetch<T extends DeviceStateEntity>(String id) async {
    T? entity = _entities[id] as T?;
    entity ??= await persistenceAdapter.fetchRecord<T>(id);
    entity != null ? _entities[id] = entity : _entities.remove(id);
    return entity;
  }

  // Maps identifier to associated entity for faster lookup
  final Map<String, DeviceStateEntity> _entities = {};
}
