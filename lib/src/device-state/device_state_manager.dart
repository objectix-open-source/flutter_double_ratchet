import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/device_record.dart';
import 'package:flutter_black_sun/src/device-state/session_record.dart';
import 'package:flutter_black_sun/src/device-state/device_state_store.dart';
import 'package:flutter_black_sun/src/device-state/user_record.dart';
import 'package:flutter_black_sun/src/sesame/session_builder.dart';

/// Implements the device state and update of this state as specified in Section
/// 3.1 and 3.2 of the Sesame specification at
/// https://signal.org/docs/specifications/sesame/
class DeviceStateManager {
  /// Session Builder to use with this instance.
  final SessionBuilder sessionBuilder;

  /// The device storage to use for data persistency on this device.
  final DeviceStateStore deviceStateStore;

  DeviceStateManager({
    required this.sessionBuilder,
    required this.deviceStateStore,
  });

  /// Deletes a User Record from the device state.
  Future<void> deleteUser(String userId) async {
    final UserRecord? deletedUser = await deviceStateStore.deleteUserRecord(userId);
    assert(deletedUser != null);
  }

  /// Deletes a Device Record from the session state.
  Future<void> deleteDevice(String deviceId) async {
    // Delete device object
    final DeviceRecord? deletedDevice = await deviceStateStore.deleteDeviceRecord(deviceId);
    assert(deletedDevice != null);

    // If this was the owning user's last device, we delete the user
    // record, too.
    final userRecord = await deviceStateStore.fetchUserRecord(
      deletedDevice!.userRecordId,
    );
    assert(userRecord != null);
    if (userRecord!.deviceRecords.isEmpty) {
      await deleteUser(userRecord.id);
    }
  }

  /// Deletes a session from the device state.
  Future<void> deleteSession(String sessionId) async {
    // Delete session object
    final SessionRecord? deletedSession = await deviceStateStore.deleteSessionRecord(sessionId);
    assert(deletedSession != null);

    // If this was the owning device' last session, we delete the device
    // record, too.
    final deviceRecord = await deviceStateStore.fetchDeviceRecord(
      deletedSession!.deviceRecordId,
    );
    assert(deviceRecord != null);
    if (deviceRecord!.sessionRecords.isEmpty) {
      await deleteDevice(deviceRecord.id);
    }
  }

  /// Inserts a new device session into the device state.
  Future<void> insertSession(SessionRecord session) async {
    // Load the denoted device record.
    final deviceRecord = await deviceStateStore.fetchDeviceRecord(session.deviceRecordId);
    assert(deviceRecord != null);

    // The session must not be already existing in the device record.
    assert(!(deviceRecord!.sessionRecords.isMember(session.id)));

    // Inserting a session makes this session always the active session
    // of the designated device. The previous active session then is put to
    // the head of the inactive sessions. This is simply accomplished by
    // inserting the new session to the beginning of the session list.
    await deviceStateStore.storeSessionRecord(session);
    deviceRecord!.sessionRecords.addHead(session);

    // If the maximum number of sessions has been reached, we cut of
    // exceeding session at the tail of the session list.
    if (deviceRecord.sessionRecords.count > 16) {
      final removedSessionId = deviceRecord.sessionRecords.removeTail();
      assert(removedSessionId != null);
      await deviceStateStore.deleteSessionRecord(removedSessionId!);
    }

    // At this point, we must store the updated device record in the store.
    await deviceStateStore.storeDeviceRecord(deviceRecord);
  }

  /// Activates an inactive session within the device state.
  Future<void> activateSession(String sessionId) async {
    // Load the denoted session.
    final session = await deviceStateStore.fetchSessionRecord(sessionId);
    assert(session != null);

    // Load the associated device record.
    final deviceRecord = await deviceStateStore.fetchDeviceRecord(
      session!.deviceRecordId,
    );
    assert(deviceRecord != null);

    // The session must be already existing in the device record, and must be
    // considered inactive.
    assert(deviceRecord!.sessionRecords.isMember(sessionId));
    assert(deviceRecord!.sessionRecords.first != sessionId);

    // Make the session active, by removing the session from the list and then
    // re-insert it at the head of the session list.
    final SessionRecord? sessionToMove = deviceRecord!.sessionRecords.removeById(
      sessionId,
    );
    assert(sessionToMove != null);
    deviceRecord.sessionRecords.addHead(sessionToMove!);

    // Write back the updated device record.
    await deviceStateStore.storeDeviceRecord(deviceRecord);
  }

  /// Marks a User Record within the device state as stale.
  Future<void> markUserAsStale(String userId) async {
    final user = await deviceStateStore.fetchUserRecord(userId);
    assert(user != null);
    await deviceStateStore.storeUserRecord(user!.copyWith(isStale: true));

    // Then go over all its devices and mark them as stale, too.
    for (final deviceId in user.deviceRecords.ids) {
      await markDeviceAsStale(deviceId);
    }
  }

  /// Marks a Device Record within the device state as stale.
  Future<void> markDeviceAsStale(String deviceId) async {
    final device = await deviceStateStore.fetchDeviceRecord(deviceId);
    assert(device != null);
    await deviceStateStore.storeDeviceRecord(device!.copyWith(isStale: true));
  }

  /// Performs a conditional update within the device state.
  Future<void> conditionalUpdate(
    Identity identity,
    SimplePublicKey publicKey,
  ) async {
    // If a relevant UserRecord does not exist, then an empty UserRecord is
    // added for this UserID.
    UserRecord? user = await deviceStateStore.fetchUserRecord(identity.user.address);
    if (user == null) {
      // UserRecord does not exist locally, create a new record.
      user = UserRecord(user: identity.user);
      await deviceStateStore.storeUserRecord(user);
    }

    // If a relevant DeviceRecord does not exist or stores an identity public
    // key that doesn't equal the input public key, then an empty DeviceRecord
    // is added for this DeviceID (replacing the previous DeviceRecord if one
    // exists). With per-device identity public keys, the input public key is stored in the
    // empty record. If the UserID and DeviceID equal the device's own values, then
    // a DeviceRecord is not added (a device doesn't add a DeviceRecord for
    // itself).
    final device = await deviceStateStore.fetchDeviceRecord(identity.id);
    if (device != null) {
      if (device.identityKey != publicKey) {
        // If device exists and identity key is different, we reset the device
        // record.
        await deviceStateStore.deleteDeviceRecord(identity.id);
        final updatedDevice = DeviceRecord(
          userRecordId: user.id,
          identity: identity,
          identityKey: publicKey,
        );
        user.deviceRecords.addTail(updatedDevice);
        await deviceStateStore.storeDeviceRecord(updatedDevice);
        await deviceStateStore.storeUserRecord(user);
      }
    } else {
      // DeviceRecord does not exist locally. Create a new record, if user
      // and/or device identifier are different from the local device'
      // identifiers.
      final localId = await deviceStateStore.fetchLocalIdentity();
      assert(localId != null);

      if (localId! != identity || localId.user != user.user) {
        final newDevice = DeviceRecord(
          userRecordId: user.id,
          identity: identity,
          identityKey: publicKey,
        );
        user.deviceRecords.addTail(newDevice);
        await deviceStateStore.storeDeviceRecord(newDevice);
        await deviceStateStore.storeUserRecord(user);
      }
    }
  }

  Future<void> prepareForEncryption(
    Identity identity,
    SimplePublicKey publicKey,
  ) async {
    // The device deletes the relevant UserRecord and/or DeviceRecord if they
    // are stale.
    final userRecord = await deviceStateStore.fetchUserRecord(identity.user.address);
    final deviceRecord = await deviceStateStore.fetchDeviceRecord(identity.id);

    if (userRecord != null && userRecord.isStale) {
      // will also delete device and sessions associated with user
      await deleteUser(userRecord.user.address);
    }

    if (deviceRecord != null && deviceRecord.isStale) {
      // will also delete sessions associated with device
      await deleteDevice(deviceRecord.identity.id);
    }

    // The device conditionally updates its records based on the tuple.
    await conditionalUpdate(identity, publicKey);

    // If the relevant DeviceRecord doesn't have an active session, then the
    // device creates a new initiating session using the relevant public key for
    // the DeviceRecord. The new session is inserted into the DeviceRecord.
    final deviceRecord2 = await deviceStateStore.fetchDeviceRecord(identity.id);

    if (deviceRecord2 != null && deviceRecord2.activeSession == null) {
      // Initiate a new secure session using device' identity key
      final newSession = await sessionBuilder.initiateNewDeviceSession(deviceRecord2);
      // insert the resulting session to the device record
      await insertSession(newSession);
    }
  }
}
