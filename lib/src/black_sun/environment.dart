import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/flutter_black_sun_test.dart';
import 'package:flutter_black_sun/src/device-state/device_state_manager.dart';
import 'package:flutter_black_sun/src/device-state/device_state_store.dart';
import 'package:flutter_black_sun/src/double_ratchet/double_ratchet.dart';
import 'package:flutter_black_sun/src/services/device_state_database_adapter.dart';
import 'package:flutter_black_sun/src/sesame/session_builder.dart';
import 'package:flutter_black_sun/src/x3dh/x3dh.dart';

/// Defines the runtime environment for BlackSun.
class Environment {
  /// Key storage service to store keys on the local device.
  final KeyStore keyStore;

  /// Key manager to handle communications with a key server.
  final KeyManager keyManager;

  /// Key provider that creates encryption and signing keys on demand.
  final KeyProvider keyProvider;

  /// Device state store for persistently storing device state.
  final DeviceStateDatabaseAdapter deviceStateDatabaseAdapter;

  /// Allows for sending and receiving secure messages to/from
  /// a messaging server.
  final MessagingService messageService;

  Environment({
    required this.keyManager,
    required this.deviceStateDatabaseAdapter,
    required this.keyProvider,
    required this.keyStore,
    required this.messageService,
  });

  /*
  static Environment createForSolar() {
    return Environment();
  }*/

  /// X3DH implementation suitable for this enviromment.
  X3DH get x3dh => _x3dh ??= X3DH(
        keyManager: keyManager,
        keyProvider: keyProvider,
        keyStore: keyStore,
      );

  /// Double Ratchet implementation for this environment.
  DoubleRatchet get doubleRatchet => _doubleRatchet ??= DoubleRatchet();

  /// Device State Store to be used with this environment.
  DeviceStateStore get deviceStateStore => _deviceStateStore ??= DeviceStateStore(
        persistenceAdapter: deviceStateDatabaseAdapter,
      );

  /// Session buuld of this environment.
  SessionBuilder get sessionBuilder => _sessionBuilder ??= SessionBuilder(
        deviceStateStore: deviceStateStore,
        x3dh: x3dh,
        doubleRatchet: doubleRatchet,
        keyStore: keyStore,
      );

  /// Device state manager to be used within this environment.
  DeviceStateManager get deviceStateManager => _deviceStateManager ??= DeviceStateManager(
        deviceStateStore: deviceStateStore,
        sessionBuilder: sessionBuilder,
      );

  /// Creates a BlackSun runtime enviroment based on mocked services, allowing
  /// for operating BlackSun without servers, e.g. within integration tests.
  static Environment createForTest(Identity identity) {
    final keyManager = MockKeyManager();
    keyManager.reset();

    final messageService = MockMessagingService(keyManager: keyManager);
    messageService.reset();

    return Environment(
      deviceStateDatabaseAdapter: MockDeviceStateDatabaseAdapter(),
      keyManager: keyManager,
      keyProvider: DefaultKeyProvider(),
      keyStore: MockKeyStore(),
      messageService: messageService,
    );
  }

  X3DH? _x3dh;
  DoubleRatchet? _doubleRatchet;
  DeviceStateStore? _deviceStateStore;
  SessionBuilder? _sessionBuilder;
  DeviceStateManager? _deviceStateManager;
}
