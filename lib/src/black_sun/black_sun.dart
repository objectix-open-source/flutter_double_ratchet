import 'dart:async';

import 'package:flutter_black_sun/flutter_black_sun.dart';

/// Implements the Black Sun facade. This is the primary API for clients of this
/// library, wrapping the Sesame algorithm, the X3DH key exchange protocol, and
/// the Double Ratchet secure messaging protocol.
class BlackSun {
  /// Sesame environment to use with this Sesame instance.
  final Environment environment;

  /// Number of one-time prekeys generated in a batch.
  final int onetimePrekeyBatchSize;

  /// Determines the maximum number of dispatch retries for an user
  final int maxNumberOfDispatchRetries;

  BlackSun({
    required this.environment,
    this.onetimePrekeyBatchSize = 16,
    this.maxNumberOfDispatchRetries = 3,
  });

  /// Adds the specified user and its current devices as correspondent.
  Future<void> addCorrespondent(User user) async {
    // We fetch the specified user's devices and associated identity keys.
    final idKeys = await environment.keyManager.fetchAllIdentityKeys(user.address);
    // Now prepare each returned device for encryption
    for (final idKey in idKeys) {
      await environment.deviceStateManager.prepareForEncryption(
        idKey.identity,
        idKey.identityKey,
      );
    }
  }

  /// Removes the specified user and its current devices as correspondent.
  Future<void> removeCorrespondent(User user) async {
    // Mark the user and all its devices as stale.
    await environment.deviceStateManager.markUserAsStale(user.address);
  }
}
