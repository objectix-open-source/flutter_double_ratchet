import 'package:equatable/equatable.dart';
import 'package:flutter_black_sun/src/black_sun/user.dart';
import 'package:flutter_black_sun/src/util/uuid_shortener.dart';
import 'package:uuid/uuid.dart';

/// Defines the identity of an BlackSun endpoint (i.e. a user agent operated on
/// a specific device of a given user).
class Identity extends Equatable {
  /// Global unique identifier for this identity. This refers to the endpoint
  /// identifier this identity is bound to.
  final String id;

  /// User descriptor denoting the user associated with this identity.
  final User user;

  /// Unique application namespace identifying the application this identity is
  /// used with (e.g. an application bundle identifier).
  final String applicationNamespace;

  const Identity({
    required this.user,
    required this.applicationNamespace,
    required this.id,
  });

  /// Creates a new identity for the given user.
  static Identity createNew(
    User forUser,
    String applicationNamespace,
  ) {
    return Identity(
      id: const Uuid().v4short(),
      user: forUser,
      applicationNamespace: applicationNamespace.toLowerCase().trim(),
    );
  }

  /// Constructs an Identity from the spevified Identity string.
  static Identity fromString(String string) {
    final tokens = string.split('/');
    assert(tokens.length == 3);
    return Identity(
      id: tokens[2],
      user: User(
        address: tokens[0],
      ),
      applicationNamespace: tokens[1],
    );
  }

  /// Converts this Identitiy into an Identity string.
  @override
  String toString() {
    return "${user.address}/$applicationNamespace/$id";
  }

  @override
  List<Object> get props => [id, applicationNamespace, user];
}
