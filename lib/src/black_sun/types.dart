import 'package:cryptography/cryptography.dart';
import 'package:equatable/equatable.dart';

/// Public part of a prekey used within BlackSun.
class PublicPrekey extends Equatable {
  /// Cryptographic public key of this prekey.
  final SimplePublicKey key;

  /// Fingerprint of this key.
  final String fingerprint;

  const PublicPrekey({
    required this.key,
    required this.fingerprint,
  });

  @override
  List<Object?> get props => [fingerprint];
}

/// Full cryptographic key pair of a prekey within BlackSun.
class Prekey extends Equatable {
  /// Cryptographic key pair of this prekey.
  final SimpleKeyPair keyPair;

  /// Fingerprint of this key.
  final String fingerprint;

  const Prekey({
    required this.keyPair,
    required this.fingerprint,
  });

  /// Extracts the public part of this prekey.
  Future<PublicPrekey> public() async {
    return PublicPrekey(
      fingerprint: fingerprint,
      key: await keyPair.extractPublicKey(),
    );
  }

  @override
  List<Object?> get props => [fingerprint];
}

/// Implements a signed prekey within BlackSun.
class SignedPrekey extends Equatable {
  /// Prekey
  final Prekey prekey;

  /// Signature for this prekey.
  final Signature signature;

  /// Fingerprint of this signed prekey.
  String get fingerprint => prekey.fingerprint;

  const SignedPrekey({
    required this.prekey,
    required this.signature,
  });

  /// Extracts the public part of this signed prekey.
  Future<SignedPublicPrekey> public() async {
    return SignedPublicPrekey(
      prekey: PublicPrekey(
        fingerprint: fingerprint,
        key: await prekey.keyPair.extractPublicKey(),
      ),
      signature: signature,
    );
  }

  @override
  List<Object?> get props => [fingerprint];
}

/// Implements the public key of a signed prekey.
class SignedPublicPrekey extends Equatable {
  /// Cryptographic public key of this signed prekey.
  final PublicPrekey prekey;

  /// Signature for this prekey.
  final Signature signature;

  /// Fingerprint of this signed prekey.
  String get fingerprint => prekey.fingerprint;

  const SignedPublicPrekey({
    required this.prekey,
    required this.signature,
  });

  @override
  List<Object?> get props => [fingerprint];
}
