import 'package:equatable/equatable.dart';
import 'package:email_validator/email_validator.dart';

/// Implements a descriptor for a BlackSun user.
class User extends Equatable {
  /// Fully qualified address for the user, e.g. "alice@example.com". This
  /// is the primary identifier for a user.
  final String address;

  User({
    required String address,
  }) : address = address.toLowerCase().trim() {
    if (!EmailValidator.validate(address)) {
      throw const FormatException("User address must be following a valid e-mail address format.");
    }
  }

  @override
  List<Object?> get props => [address];

  /// Returns the address of this user.
  @override
  String toString() {
    return address;
  }
}
