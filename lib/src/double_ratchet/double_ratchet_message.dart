import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';
import '../util/data_input_stream.dart';
import '../util/data_output_stream.dart';

/// Implements the double ratchet message header preceding the ciphertext of
/// an encrypted message.
class DoubleRatchetMessageHeader {
  final SimplePublicKey publicDhKey;
  final int previousChainLength;
  final int messageNumber;

  DoubleRatchetMessageHeader({
    required this.publicDhKey,
    required this.previousChainLength,
    required this.messageNumber,
  });

  void toOutputStream(DataOutputStream outputStream) {
    outputStream.writeBytes(
      Uint8List.fromList(
        publicDhKey.bytes,
      ),
    );
    outputStream.writeInt(messageNumber);
    outputStream.writeInt32(previousChainLength);
  }

  static DoubleRatchetMessageHeader fromInputStream(
    DataInputStream inputStream,
  ) {
    final publicDhKeyBytes = inputStream.readBytes();
    final publicDhKey = SimplePublicKey(
      publicDhKeyBytes,
      type: KeyPairType.x25519,
    );
    final messageNumber = inputStream.readInt();
    final prevChainLength = inputStream.readInt32();
    return DoubleRatchetMessageHeader(
      publicDhKey: publicDhKey,
      previousChainLength: prevChainLength,
      messageNumber: messageNumber,
    );
  }
}

/// Encapsulates a single message produced by the double ratchet encryption
/// algorithm.
class DoubleRatchetMessage {
  /// Header of this message
  final DoubleRatchetMessageHeader header;

  /// Encrypted payload (ciphertext) of this message
  final SecretBox ciphertext;

  DoubleRatchetMessage({
    required this.header,
    required this.ciphertext,
  });

  void toOutputStream(DataOutputStream stream) {
    header.toOutputStream(stream);
    ciphertext.toOutputStream(stream);
  }

  static DoubleRatchetMessage fromInputStream(DataInputStream stream) {
    return DoubleRatchetMessage(
      header: DoubleRatchetMessageHeader.fromInputStream(stream),
      ciphertext: _SecretBoxSerialization.fromInputStream(stream),
    );
  }
}

extension _SecretBoxSerialization on SecretBox {
  void toOutputStream(DataOutputStream outputStream) {
    outputStream.writeBytes(Uint8List.fromList(mac.bytes));
    outputStream.writeBytes(Uint8List.fromList(nonce));
    outputStream.writeBytes(Uint8List.fromList(cipherText));
  }

  static SecretBox fromInputStream(DataInputStream inputStream) {
    final macBytes = inputStream.readBytes();
    final nonceBytes = inputStream.readBytes();
    final ciphertextBytes = inputStream.readBytes();
    return SecretBox(
      ciphertextBytes,
      nonce: nonceBytes,
      mac: Mac(macBytes),
    );
  }
}
