class InvalidMessageException implements Exception {
  const InvalidMessageException() : super();
}

class UnknownMailboxException implements Exception {
  const UnknownMailboxException() : super();
}

class IdentityServerException implements Exception {
  final String? message;
  const IdentityServerException(this.message) : super();
}
