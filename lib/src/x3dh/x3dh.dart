import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';

/// Defines the private output from a X3DH protocol invocation. The data herein
/// must be kept private by the local identity.
class X3DHPrivateParameters {
  final SecretKey sharedKey;
  final List<int> associatedData;

  X3DHPrivateParameters({
    required this.sharedKey,
    required this.associatedData,
  });
}

/// Defines the public output from a X3DH protocol invocation. The data herein
/// can be shared over unsecure channels without compromise of security.
class X3DHPublicParameters {
  final SimplePublicKey initiatingIdentityKey;
  final SimplePublicKey remoteIdentityKey;
  final SimplePublicKey ephemeralKey;
  final String remotePrekeyId;
  final SignedPublicPrekey remotePrekey;
  final String? onetimePrekeyId;

  X3DHPublicParameters({
    required this.initiatingIdentityKey,
    required this.remoteIdentityKey,
    required this.ephemeralKey,
    required this.remotePrekeyId,
    required this.remotePrekey,
    this.onetimePrekeyId,
  });
}

/// Defines the output from a single X3DH protocol run, when initiated by the
/// local party. Consists of both a public and a private data part.
/// Protocol parameters are fixed to Curve25519 for ECDH and ECDSA, using a
/// SHA-512 hash for KDF.
class X3DHOutput {
  final X3DHPrivateParameters privateParameters;
  final X3DHPublicParameters publicParameters;

  X3DHOutput({
    required this.privateParameters,
    required this.publicParameters,
  });
}

/// Implements the core of the X3DH Key Agreement Protocol as specified in
/// https://signal.org/docs/specifications/x3dh/
class X3DH {
  final KeyStore keyStore;
  final KeyProvider keyProvider;
  final KeyManager keyManager;

  X3DH({
    required this.keyStore,
    required this.keyProvider,
    required this.keyManager,
  });

  /// Executes the X3DH protocol as initiator. This results a X3DH output, which
  /// then can be applied to any post-X3DH protocol for communication.
  Future<X3DHOutput> initiateProtocol(
    Identity localIdentity,
    Identity remoteIdentity,
  ) async {
    // (1) Initiator needs to fetch the key package of the remote identity
    final prekeyBundle = await keyManager.fetchPrekeyBundle(remoteIdentity);

    // (2) Initiator then needs to verify the signature of the prekey in the
    // downloaded key package.
    final remoteIdentityKey = prekeyBundle.identityKey.bytes;
    final sig = Ed25519();
    final signatureValid = await sig.verify(
      prekeyBundle.signedPrekey.prekey.key.bytes,
      signature: prekeyBundle.signedPrekey.signature,
    );
    // Signature must be valid in order to progress with protocol
    assert(signatureValid);

    // (3) Initiator now generates ephemeral key pair
    final ephemeralKey = await keyProvider.generateKey();

    // (4) Initiator now calculates a shared key by section 3.3 of X3DH
    final identityKey = await keyStore.fetchIdentityKey();
    assert(identityKey != null);

    final dh1 = await _dh(
      identityKey!,
      prekeyBundle.signedPrekey.prekey.key,
    );
    final dh2 = await _dh(
      ephemeralKey,
      prekeyBundle.identityKey,
    );
    final dh3 = await _dh(
      ephemeralKey,
      prekeyBundle.signedPrekey.prekey.key,
    );

    SecretKey sharedKey;
    String? otkId;

    if (prekeyBundle.onetimePrekey == null) {
      // no onetime prekey available
      sharedKey = await _kdf(
        (await dh1.extractBytes()) + (await dh2.extractBytes()) + (await dh3.extractBytes()),
        localIdentity.applicationNamespace.codeUnits,
      );
    } else {
      // onetime prekey available, use it in protocol then
      final otk = prekeyBundle.onetimePrekey!.key;
      otkId = prekeyBundle.onetimePrekey!.fingerprint;
      final dh4 = await _dh(
        ephemeralKey,
        otk,
      );
      sharedKey = await _kdf(
        (await dh1.extractBytes()) +
            (await dh2.extractBytes()) +
            (await dh3.extractBytes()) +
            (await dh4.extractBytes()),
        localIdentity.applicationNamespace.codeUnits,
      );
    }

    // (5) Initiator then calculates Associated Data
    final ad = (await identityKey.extractPublicKey()).bytes + remoteIdentityKey;

    // We are done with X3DH now.
    return X3DHOutput(
      privateParameters: X3DHPrivateParameters(
        sharedKey: sharedKey,
        associatedData: ad,
      ),
      publicParameters: X3DHPublicParameters(
        initiatingIdentityKey: await identityKey.extractPublicKey(),
        remoteIdentityKey: prekeyBundle.identityKey,
        ephemeralKey: await ephemeralKey.extractPublicKey(),
        remotePrekeyId: prekeyBundle.signedPrekey.fingerprint,
        remotePrekey: prekeyBundle.signedPrekey,
        onetimePrekeyId: otkId,
      ),
    );
  }

  // Completes the X3DH protocol on the remote side. Returns the shared key
  // and associated data established from the X3DH protocol.
  Future<X3DHPrivateParameters> completeProtocol(
    Identity localIdentity,
    Identity initiatingIdentity,
    X3DHPublicParameters wireData,
  ) async {
    final identityKey = await keyStore.fetchIdentityKey();
    assert(identityKey != null);
    final prekey = await keyStore.fetchSignedPrekey(wireData.remotePrekeyId);
    assert(prekey != null);
    final otk = wireData.onetimePrekeyId != null ? await keyStore.fetchOnetimePrekey(wireData.onetimePrekeyId!) : null;

    final dh1 = await _dh(
      prekey!.prekey.keyPair,
      wireData.initiatingIdentityKey,
    );
    final dh2 = await _dh(
      identityKey!,
      wireData.ephemeralKey,
    );
    final dh3 = await _dh(
      prekey.prekey.keyPair,
      wireData.ephemeralKey,
    );

    SecretKey sharedKey;

    if (otk == null) {
      sharedKey = await _kdf(
        (await dh1.extractBytes()) + (await dh2.extractBytes()) + (await dh3.extractBytes()),
        localIdentity.applicationNamespace.codeUnits,
      );
    } else {
      final dh4 = await _dh(
        otk.keyPair,
        wireData.ephemeralKey,
      );
      sharedKey = await _kdf(
        (await dh1.extractBytes()) +
            (await dh2.extractBytes()) +
            (await dh3.extractBytes()) +
            (await dh4.extractBytes()),
        localIdentity.applicationNamespace.codeUnits,
      );
    }

    final ad = wireData.initiatingIdentityKey.bytes + (await identityKey.extractPublicKey()).bytes;

    return X3DHPrivateParameters(
      sharedKey: sharedKey,
      associatedData: ad,
    );
  }

  Future<SecretKey> _dh(
    SimpleKeyPair dhKeyPair,
    SimplePublicKey publicDhKey,
  ) async {
    final dh = X25519();
    return dh.sharedSecretKey(
      keyPair: dhKeyPair,
      remotePublicKey: publicDhKey,
    );
  }

  Future<SecretKey> _kdf(List<int> key, List<int> info) async {
    final input = List<int>.filled(32, 0xff) + key;
    final kdf = Hkdf(hmac: Hmac.sha512(), outputLength: 32);
    final nonce = List<int>.filled(kdf.hmac.macLength, 0x00);
    final secretKey = SecretKey(input);
    return kdf.deriveKey(secretKey: secretKey, nonce: nonce, info: info);
  }
}
