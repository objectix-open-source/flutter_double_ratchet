import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/src/black_sun/identity.dart';
import 'package:flutter_black_sun/src/black_sun/types.dart';

/// Defines the container for a single device identity fetched by this service.
class IdentityKey {
  /// The identity of the device that owns this idenity key,
  final Identity identity;

  /// The identity key of a device, whose identity is denoted in "identity".
  final SimplePublicKey identityKey;

  IdentityKey({
    required this.identity,
    required this.identityKey,
  });
}

/// Defines the container for key packages communicated between
/// client and server.
class KeyPackage {
  /// Identity this key package is owned by
  final Identity identity;

  /// Public identity key of the owning identity.
  final SimplePublicKey identityKey;

  /// The signing key of the owning identity.
  final SimplePublicKey signingKey;

  /// Public prekey of the owning identity.
  final SignedPublicPrekey signedPrekey;

  /// The list of one-time prekeys in this key package.
  final List<PublicPrekey> onetimePrekeys;

  KeyPackage({
    required this.identity,
    required this.identityKey,
    required this.signingKey,
    required this.signedPrekey,
    required this.onetimePrekeys,
  });
}

/// Defines the container for "prekey bundles" communicated between
/// client and server.
class PrekeyBundle {
  /// Identity which owns this prekey bundle.
  final Identity identity;

  /// Identity key for the denoted identity.
  final SimplePublicKey identityKey;

  /// Signing key of the denoted identity.
  final SimplePublicKey signingKey;

  /// Prekey of the owning identity.
  final SignedPublicPrekey signedPrekey;

  /// Optional one-time prekey. may be null, if no such prekey was available.
  final PublicPrekey? onetimePrekey;

  PrekeyBundle({
    required this.identity,
    required this.identityKey,
    required this.signingKey,
    required this.signedPrekey,
    this.onetimePrekey,
  });
}

/// Declares an interface for X3DH key managers.
abstract class KeyManager {
  /// Publishes a new key package to a X3DH server, potentially replacing
  /// any keypackage for the denoted identity.
  Future<void> publish(KeyPackage keyPackage);

  /// Invalidates the key package for the given user and device, effectively
  /// removing the key package from the server. Does nothing, if the local
  /// identity has no key package published on the server.
  Future<void> invalidate(Identity identity);

  /// Fetches the a key package for the given identity from the X3DH server, if
  /// such a package is existing there.
  Future<KeyPackage> fetchKeyPackage(Identity identity);

  /// Fetches a "prekey bundle" for the given identity, if such an identity is
  /// known.
  Future<PrekeyBundle> fetchPrekeyBundle(Identity identity);

  /// Fetches the identity key for the given identity.
  Future<SimplePublicKey> fetchIdentityKey(Identity identity);

  /// Fetches all device identity keys for the given user.
  Future<List<IdentityKey>> fetchAllIdentityKeys(String userId);
}
