import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';

/// Implements the model for a message that is to be dispatched by
/// the message service.
class MessageServiceOutboundMessage {
  /// Identifier of device endpoint (aka. Identity) this message is
  /// to be delivered to.
  final String toDeviceId;

  /// Binary ciphertext message content, encrypted by BlackSun.
  final Uint8List payload;

  MessageServiceOutboundMessage({
    required this.toDeviceId,
    required this.payload,
  });
}

/// Impements the model for a message delivered by the message service.
class MessageServiceInboundMessage {
  /// Identity of the sending device endpoint.
  final Identity fromIdentity;

  /// Binary plaintext message content, decrypted by BlackSun.
  final Uint8List payload;

  MessageServiceInboundMessage({
    required this.fromIdentity,
    required this.payload,
  });
}

/// Results for a message dispatch request.
enum MessageServiceDispatchResult {
  /// Server accepted dispatch request
  accepted,

  /// user specified in dispatch request was unknown
  userUnknown,

  /// recipient device identifiers specified in dispatch request were not current
  recipientDevicesNotCurrent,
}

/// Encapsulates a Dispatch response from a message server.
class MessageServerDispatchResponse {
  /// Result for the dispatch request
  final MessageServiceDispatchResult result;

  /// If "result" is "recipientDevicesNotCurrent", this may contain the list of
  /// device ids that need to be ADDED to the recipient list in order to make
  /// the server accept the next dispatch for that user.
  final List<Identity> newDevices;

  /// If "result" is "recipientDevicesNotCurrent", this may contain the list of
  /// public identity keys for each corresponding device identifier contained in
  /// "newDevices".
  final List<SimplePublicKey> newDevicePublicKeys;

  /// If "result" is "recipientDevicesNotCurrent", this may contain the list of
  // device ids that need to be REMOVED from the recipient list in order to make
  // the server accept the next dispatch for that user.
  final List<Identity> obsoleteDevices;

  MessageServerDispatchResponse({
    required this.result,
    required this.newDevices,
    required this.newDevicePublicKeys,
    required this.obsoleteDevices,
  });
}

/// Interface for implementations of a message service that sends messages
/// to and fetches messages from remote session endpoint.
abstract class MessagingService {
  /// Registers the local user and device with the messaging server, essentially
  /// setting up a new mailbox, if such a mailbox does not exist yet.
  /// The server operation is expected to be idempotent.
  Future<void> register(Identity identity);

  /// Connects this service to the local device's mailbox, resulting in any
  /// pending messages in the connected mailbox to be deliverd to this service.
  /// Returns a stream of inbound messages, intended for the local identity
  /// passed to this function.
  Future<Stream<MessageServiceInboundMessage>> connect(Identity identity);

  /// Sends messages to the corresponding device mailboxes of the given user.
  Future<MessageServerDispatchResponse> dispatch(
    List<MessageServiceOutboundMessage> messagesForDevices,
    User toUser,
    Identity senderIdentity,
  );
}
