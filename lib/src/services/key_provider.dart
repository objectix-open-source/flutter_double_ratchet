import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/src/black_sun/types.dart';

/// Interface for a key provider that can provide client with fresh
/// X3DH keys.
abstract class KeyProvider {
  /// Asks this provider for a new x25519 keypair.
  Future<SimpleKeyPair> generateKey();

  /// Asks this provider for a new x25519-based prekey.
  Future<Prekey> generatePrekey();

  /// Asks this provider for a new ed25519 signing keypair suitable for signing.
  Future<SimpleKeyPair> generateSigningKey();

  /// Asks this provider for a new prekey that is signed with the specified
  /// signing key.
  Future<SignedPrekey> generateSignedPrekey(
    SimpleKeyPair signingKey,
  );
}

class DefaultKeyProvider implements KeyProvider {
  final _x25519 = X25519();
  final _ed25519 = Ed25519();

  @override
  Future<SimpleKeyPair> generateKey() async {
    return await _x25519.newKeyPair();
  }

  @override
  Future<Prekey> generatePrekey() async {
    final key = await _x25519.newKeyPair();
    return Prekey(
      keyPair: key,
      fingerprint: await _fingerprint(key),
    );
  }

  @override
  Future<SimpleKeyPair> generateSigningKey() async {
    return await _ed25519.newKeyPair();
  }

  @override
  Future<SignedPrekey> generateSignedPrekey(SimpleKeyPair signingKey) async {
    final key = await generatePrekey();
    final signature = await _ed25519.sign(
      (await key.keyPair.extractPublicKey()).bytes,
      keyPair: signingKey,
    );
    return SignedPrekey(prekey: key, signature: signature);
  }

  Future<String> _fingerprint(SimpleKeyPair keyPair) async {
    final pubKey = await keyPair.extractPublicKey();
    return base64Url.encode(sha256.convert(pubKey.bytes).bytes).replaceAll('=', '');
  }
}
