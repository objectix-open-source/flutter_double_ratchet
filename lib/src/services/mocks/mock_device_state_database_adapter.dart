import 'dart:collection';

import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/types.dart';
import 'package:flutter_black_sun/src/services/device_state_database_adapter.dart';

class MockDeviceStateDatabaseAdapter implements DeviceStateDatabaseAdapter {
  // Just remember all objects in memory, index by associated id
  final Map<String, DeviceStateEntity> _records = HashMap();
  Identity? _identity;

  @override
  Future<void> reset() async {
    _records.clear();
    _identity = null;
  }

  @override
  Future<void> storeRecord(DeviceStateEntity record) async {
    _records[record.id] = record;
  }

  @override
  Future<void> storeLocalIdentity(Identity identity) async {
    _identity = identity;
  }

  @override
  Future<T?> fetchRecord<T extends DeviceStateEntity>(String id) async {
    return _records[id] as T?;
  }

  @override
  Future<Identity?> fetchLocalIdentity() async {
    return _identity;
  }

  @override
  Future<T?> deleteRecord<T extends DeviceStateEntity>(String id) async {
    return _records.remove(id) as T?;
  }

  @override
  Future<Identity?> deleteLocalIdentity() async {
    final uid = _identity;
    _identity = null;
    return uid;
  }
}
