// Mock implementation for X3DH key publisher relying on a mocked server.
import 'dart:collection';

import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/exceptions/exceptions.dart';
import 'package:flutter_black_sun/src/services/key_manager.dart';

class MockKeyManager implements KeyManager {
  // Maps X3DH identity to the corresponding key package as maintained by
  // mocked server.
  static final HashMap<String, KeyPackage> _keyPackageMap = HashMap<String, KeyPackage>();

  void reset() {
    _keyPackageMap.clear();
  }

  @override
  Future<void> publish(KeyPackage keyPackage) async {
    _keyPackageMap[keyPackage.identity.id] = keyPackage;
  }

  @override
  Future<void> invalidate(
    Identity identity,
  ) async {
    _keyPackageMap.remove(identity.id);
  }

  @override
  Future<KeyPackage> fetchKeyPackage(
    Identity identity,
  ) async {
    // Server responds with only one OTK, deleting the returned OTK from its
    // database.
    final kp = _keyPackageMap[identity.id];

    if (kp != null) {
      return kp;
    } else {
      throw const IdentityServerException("Identity unknown to server");
    }
  }

  @override
  Future<PrekeyBundle> fetchPrekeyBundle(
    Identity identity,
  ) async {
    // Server responds with only one OTK, deleting the returned OTK from its
    // database.
    final kp = _keyPackageMap[identity.id];

    if (kp != null) {
      final otk = kp.onetimePrekeys.isNotEmpty ? kp.onetimePrekeys.removeAt(0) : null;
      final result = PrekeyBundle(
        identity: kp.identity,
        identityKey: kp.identityKey,
        signingKey: kp.signingKey,
        signedPrekey: kp.signedPrekey,
        onetimePrekey: otk,
      );
      return result;
    } else {
      throw const IdentityServerException("Identity unknown to server");
    }
  }

  @override
  Future<SimplePublicKey> fetchIdentityKey(Identity identity) async {
    final kp = _keyPackageMap[identity.id];

    if (kp != null) {
      return kp.identityKey;
    } else {
      throw const IdentityServerException("Identity unknown to server");
    }
  }

  @override
  Future<List<IdentityKey>> fetchAllIdentityKeys(String userId) async {
    // Go through all keypackages and collect the identity key from each.
    List<IdentityKey> identityKeysForUser = [];
    for (var keypackage in _keyPackageMap.values) {
      if (keypackage.identity.user.address == userId) {
        identityKeysForUser.add(
          IdentityKey(
            identity: keypackage.identity,
            identityKey: keypackage.identityKey,
          ),
        );
      }
    }

    return identityKeysForUser;
  }
}
