import 'dart:collection';

import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';

/// Implements key storage interface using an in-memory database.
class MockKeyStore implements KeyStore {
  // Map signed prekeys id to key
  final _signedPrekeyMap = HashMap<String, SignedPrekey>();
  // Map one-time prekeys id to key
  final _oneTimePrekeyMap = HashMap<String, Prekey>();
  // Current active signed prekey
  SignedPrekey? _activeSignedPrekey;
  // The current identity key
  SimpleKeyPair? _identityKey;
  // The current signing key
  SimpleKeyPair? _signingKey;

  @override
  Future<void> deleteOnetimePrekey(String id) async {
    _oneTimePrekeyMap.remove(id);
  }

  @override
  Future<void> deleteSignedPrekey(String id) async {
    _signedPrekeyMap.remove(id);
  }

  @override
  Future<SimpleKeyPair?> fetchIdentityKey() async {
    return _identityKey;
  }

  @override
  Future<SimpleKeyPair?> fetchSigningKey() async {
    return _signingKey;
  }

  @override
  Future<Prekey?> fetchOnetimePrekey(String id) async {
    return _oneTimePrekeyMap[id];
  }

  @override
  Future<List<Prekey>> fetchOnetimePrekeys() async {
    return _oneTimePrekeyMap.values.toList();
  }

  @override
  Future<SignedPrekey?> fetchSignedPrekey(String id) async {
    return _signedPrekeyMap[id];
  }

  @override
  Future<SignedPrekey?> fetchActiveSignedPrekey() async {
    return _activeSignedPrekey;
  }

  @override
  Future<void> storeIdentityKey(SimpleKeyPair key) async {
    _identityKey = key;
  }

  @override
  Future<void> storeSigningKey(SimpleKeyPair key) async {
    _signingKey = key;
  }

  @override
  Future<void> storeOneTimePrekeys(List<Prekey> keys) async {
    for (Prekey key in keys) {
      _oneTimePrekeyMap[key.fingerprint] = key;
    }
  }

  @override
  Future<void> storeSignedPrekey(SignedPrekey key) async {
    _signedPrekeyMap[key.fingerprint] = key;
  }

  @override
  Future<SignedPrekey?> activateSignedPrekey(String fingerprint) async {
    _activeSignedPrekey = _signedPrekeyMap[fingerprint];
    return _activeSignedPrekey;
  }
}
