import 'dart:async';
import 'dart:collection';

import 'package:collection/collection.dart';
import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/exceptions/exceptions.dart';

/// Mock implementation for the Message Service
class MockMessagingService implements MessagingService {
  // Mocks the central messaging server with mailboxes for each user
  // and device. Key = user-address/device-id
  static final _mailboxes = HashMap<String, _Mailbox>();

  // We connect the mock messaging service with the (mock) key manager service,
  // so we can fetch public keys for devices from there.
  final KeyManager keyManager;

  MockMessagingService({
    required this.keyManager,
  });

  void reset() {
    _mailboxes.clear();
  }

  @override
  Future<void> register(Identity identity) async {
    // Setup a default (empty) mailbox for the current user/device
    _mailboxes[identity.toString()] = _Mailbox();
  }

  @override
  Future<Stream<MessageServiceInboundMessage>> connect(Identity identity) async {
    final mailbox = _mailboxes[identity.toString()];
    if (mailbox != null) {
      mailbox.isConnected = true;
      return mailbox.streamController.stream.asBroadcastStream();
    } else {
      throw const UnknownMailboxException();
    }
  }

  @override
  Future<MessageServerDispatchResponse> dispatch(
    List<MessageServiceOutboundMessage> messages,
    User toUser,
    Identity senderIdentity,
  ) async {
    // Check, if the user id is known
    final mailboxIds = _mailboxes.keys;
    final map = HashMap<String, List<String>>();
    for (final mailboxId in mailboxIds) {
      final tokens = mailboxId.split('/');
      final devices = map.putIfAbsent(tokens[0], () => []);
      // Filter out device ids for other application namespaces
      if (tokens[1] == senderIdentity.applicationNamespace) {
        devices.add(tokens[2]);
      }
    }

    final deviceIdsForRecipient = map[toUser.address]?.toSet();
    if (deviceIdsForRecipient == null) {
      // user is not known -> reject request
      return MessageServerDispatchResponse(
        result: MessageServiceDispatchResult.userUnknown,
        newDevices: [],
        newDevicePublicKeys: [],
        obsoleteDevices: [],
      );
    }

    // Otherwise check if the devicelist specified in send request equals
    // to the currently known list for that user.
    final sentDeviceIds = messages.map((e) => e.toDeviceId).toSet();
    final deviceIdsMissingInRequest = deviceIdsForRecipient.difference(sentDeviceIds);
    final unknownDeviceIdsInRequest = sentDeviceIds.difference(deviceIdsForRecipient);

    if (deviceIdsMissingInRequest.isEmpty && unknownDeviceIdsInRequest.isEmpty) {
      // Sent device ids are current -> accept request
      for (final msg in messages) {
        final mailbox = _mailboxes.putIfAbsent(
          "${toUser.address}/${senderIdentity.applicationNamespace}/${msg.toDeviceId}",
          () => _Mailbox(),
        );

        final inMessage = MessageServiceInboundMessage(
          fromIdentity: senderIdentity,
          payload: msg.payload,
        );

        mailbox.messages.add(inMessage);

        // Emit signal for new message, if mailbox is connected.
        if (mailbox.isConnected) {
          mailbox.streamController.add(inMessage);
        }
      }

      return MessageServerDispatchResponse(
        result: MessageServiceDispatchResult.accepted,
        newDevices: [],
        newDevicePublicKeys: [],
        obsoleteDevices: [],
      );
    } else {
      // Requested device id list not current. Thus, we reject the
      // request and tell the client, what needs to be changed in order to
      // make the list of devices current.

      // Fetch the public identity keys of the new devices.
      final pubKeys = await Future.wait(deviceIdsMissingInRequest.map(
        (deviceId) async {
          final identity = Identity(
            id: deviceId,
            user: toUser,
            applicationNamespace: senderIdentity.applicationNamespace,
          );
          return await keyManager.fetchIdentityKey(identity);
        },
      ));

      final newDevices = deviceIdsMissingInRequest.map((id) => Identity.fromString(id));
      final obsoleteDevices = unknownDeviceIdsInRequest.map((id) => Identity.fromString(id));

      return MessageServerDispatchResponse(
        result: MessageServiceDispatchResult.recipientDevicesNotCurrent,
        newDevices: newDevices.toList(growable: false),
        newDevicePublicKeys: pubKeys.whereNotNull().toList(growable: false),
        obsoleteDevices: obsoleteDevices.toList(growable: false),
      );
    }
  }
}

class _Mailbox {
  // List of current messages
  final List<MessageServiceInboundMessage> messages = [];
  // Stream Controller simulating message pushs
  final streamController = StreamController<MessageServiceInboundMessage>();
  // Is this mailbox currently connected?
  bool isConnected = false;
}
