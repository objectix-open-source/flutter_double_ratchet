import 'package:cryptography/cryptography.dart';
import 'package:flutter_black_sun/src/black_sun/types.dart';

/// Declares the interface for a key storage used during X3DH protocol.
/// Implementations intended for production should store all material in
/// a secure manner, e.g. by encrypting all data before storing on a storage
/// medium.
abstract class KeyStore {
  /// Stores given identity key.
  Future<void> storeIdentityKey(SimpleKeyPair key);

  /// Stores given signing key.
  Future<void> storeSigningKey(SimpleKeyPair key);

  /// Stores given signed prekey.
  Future<void> storeSignedPrekey(SignedPrekey key);

  /// Activates the signed prekey, deactivating the previously activated
  /// prekey (if any). Returns the activated prekey (if found by fingerprint).
  Future<SignedPrekey?> activateSignedPrekey(String fingerprint);

  /// Stores given one-time prekeys.
  Future<void> storeOneTimePrekeys(List<Prekey> keys);

  /// Fetches the identity key.
  Future<SimpleKeyPair?> fetchIdentityKey();

  /// Fetches the signing key.
  Future<SimpleKeyPair?> fetchSigningKey();

  /// Fetches a signed prekey given by its fingerprint.
  Future<SignedPrekey?> fetchSignedPrekey(String fingerprint);

  /// Fetches the currently active signed prekey.
  Future<SignedPrekey?> fetchActiveSignedPrekey();

  /// Fetches a one-time prekey given by its fingerprint.
  Future<Prekey?> fetchOnetimePrekey(String fingerprint);

  /// Fetches all available one-time prekeys.
  Future<List<Prekey>> fetchOnetimePrekeys();

  /// Deletes a given signed prekey.
  Future<void> deleteSignedPrekey(String fingerprint);

  /// Deletes a given one-time prekey.
  Future<void> deleteOnetimePrekey(String fingerprint);
}
