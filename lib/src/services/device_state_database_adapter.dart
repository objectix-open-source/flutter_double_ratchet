import 'package:flutter_black_sun/flutter_black_sun.dart';
import 'package:flutter_black_sun/src/device-state/types.dart';

/// Interface for a device state persistence adapter. This adapter is
/// responsible for persisently storing UserRecord, DeviceRecord, SessionRecord
/// and the local identity to a persistent database. Referential integrity
/// and links between entities are NOT maintained by this adapter.
abstract class DeviceStateDatabaseAdapter {
  /// Stores a record in persistent storage.
  Future<void> storeRecord(DeviceStateEntity entity);

  /// Stores the specified identity as local identity in persistent storage.
  Future<void> storeLocalIdentity(Identity identity);

  /// Fetches a record from persistent storage.
  Future<T?> fetchRecord<T extends DeviceStateEntity>(String recordId);

  /// Fetches the local identity from persistent storage.
  Future<Identity?> fetchLocalIdentity();

  /// Deletes a record from persistent storage.
  Future<T?> deleteRecord<T extends DeviceStateEntity>(String recordId);

  /// Deletes the local identity from persistent storage.
  Future<Identity?> deleteLocalIdentity();

  /// Resets the persistent storage, removing all records and local identities
  /// from the persistent storage.
  Future<void> reset();
}
