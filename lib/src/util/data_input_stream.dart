import 'dart:convert';
import 'dart:typed_data';

/// Reads structured data from a binary buffer.
class DataInputStream {
  final Uint8List buffer;
  int _offset = 0;

  DataInputStream({required this.buffer});

  /// Reads a 32-bit signed integer from the underlying buffer's current read position.
  int readInt32() {
    assert(buffer.length - _offset >= 4);
    final result = buffer.buffer.asByteData().getInt32(_offset, Endian.big);
    _offset += 4;
    return result;
  }

  /// Reads a 64-bit signed integer from the underlying buffer's current read position.
  int readInt() {
    assert(buffer.length - _offset >= 8);
    final result = buffer.buffer.asByteData().getInt64(_offset, Endian.big);
    _offset += 8;
    return result;
  }

  /// Reads a UTF-8 string from the underlying buffer's current read position.
  String readString() {
    // Read length first
    final length = readInt32();
    // Read string data (as UTF-8)
    assert(buffer.length - _offset >= length);
    final stringData = buffer.buffer.asUint8List(_offset, length);
    _offset += length;
    return utf8.decode(stringData);
  }

  /// Reads a byte array from the underlying buffer's current read position.
  Uint8List readBytes() {
    // Read length first
    final length = readInt32();
    // Read byte data
    assert(buffer.length - _offset >= length);
    final bytes = buffer.buffer.asUint8List(_offset, length);
    _offset += length;
    return bytes;
  }

  /// Reads a boolean value from the underlying buffer's current read position.
  bool readBool() {
    assert(buffer.length - _offset >= 1);
    final result = buffer.buffer.asByteData().getUint8(_offset);
    _offset++;
    return result == 1;
  }

  /// Reads a single-precision float value from the underlying buffer's current read position.
  double readFloat() {
    assert(buffer.length - _offset >= 4);
    final result = buffer.buffer.asByteData().getFloat32(_offset, Endian.big);
    _offset += 4;
    return result;
  }

  /// Reads a double precision float value from the underlying buffer's current read position.
  double readDouble() {
    assert(buffer.length - _offset >= 8);
    final result = buffer.buffer.asByteData().getFloat64(_offset, Endian.big);
    _offset += 8;
    return result;
  }

  /// Reads a singl byte value from the underlying buffer's current read position.
  int readByte() {
    assert(buffer.length - _offset >= 1);
    final result = buffer.buffer.asByteData().getUint8(_offset);
    _offset++;
    return result;
  }
}
