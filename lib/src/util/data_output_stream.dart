import 'dart:convert';
import 'dart:typed_data';

/// Writes structured data to a binary buffer.
class DataOutputStream {
  final List<Uint8List> _buffer = [];

  /// Resets the underlying binary buffer, eventually deleting any data that has been written to it.
  void reset() {
    _buffer.clear();
  }

  /// Writes a byte array to the underlying byte buffer, starting at the current write position.
  void writeBytes(Uint8List bytes) {
    // Write length of bytes first
    writeInt32(bytes.length);
    // Write bytes
    _buffer.add(bytes);
  }

  /// Writes a UTF-8 string to the underlying byte buffer, starting at the current write position.
  void writeString(String string) {
    // Write length of string first
    writeInt32(string.length);
    // Write utf-8 string data
    Uint8List bytes = utf8.encode(string) as Uint8List;
    _buffer.add(bytes);
  }

  /// Writes a signed 32-bit integer to the underlying byte buffer, starting at the current write position.
  void writeInt32(int integer) {
    // Write 32bit integer in network order
    final bytes = Uint8List(4)..buffer.asByteData().setInt32(0, integer, Endian.big);
    _buffer.add(bytes);
  }

  /// Writes a signed 64-bit integer to the underlying byte buffer, starting at the current write position.
  void writeInt(int integer) {
    // Write 64bit integer in network order
    final bytes = Uint8List(8)..buffer.asByteData().setInt64(0, integer, Endian.big);
    _buffer.add(bytes);
  }

  /// Writes a single-precision float value to the underlying byte buffer,
  /// starting at the current write position.
  void writeFloat(double float) {
    final bytes = Uint8List(4)..buffer.asByteData().setFloat32(0, float, Endian.big);
    _buffer.add(bytes);
  }

  /// Writes a double-precision float value to the underlying byte buffer,
  /// starting at the current write position.
  void writeDouble(double double) {
    final bytes = Uint8List(8)..buffer.asByteData().setFloat64(0, double, Endian.big);
    _buffer.add(bytes);
  }

  /// Writes a bool value to the underlying byte buffer, starting at the current write position.
  void writeBool(bool bool) {
    final bytes = Uint8List(1)..buffer.asByteData().setUint8(0, bool ? 1 : 0);
    _buffer.add(bytes);
  }

  /// Writes a single byte value to the underlying byte buffer, starting at the current write position.
  void writeByte(int byte) {
    final bytes = Uint8List(1)..buffer.asByteData().setUint8(0, byte);
    _buffer.add(bytes);
  }

  /// Returns the content of the binary buffer that is backing this stream.
  Uint8List get byteBuffer {
    Uint8List result = Uint8List(length);
    int offset = 0;
    for (final item in _buffer) {
      result.setAll(offset, item);
      offset += item.length;
    }
    return result;
  }

  /// Returns the current length of the binary buffer (in bytes).
  int get length {
    int sum = 0;
    for (final item in _buffer) {
      sum += item.length;
    }
    return sum;
  }
}
