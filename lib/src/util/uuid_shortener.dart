import 'dart:convert';

import 'package:uuid/uuid.dart';

/// Implements a UUIDv4 generator that generates a fresh UUID value and
/// shortens its string representation by apllying a URL-safe base64 encoding
/// with removed padding.
extension UuidShortener on Uuid {
  static final _buffer = List.filled(16, 0, growable: false);
  String v4short() {
    final uuid = v4buffer(_buffer);
    return base64Url.encode(uuid).replaceAll('=', '');
  }
}
